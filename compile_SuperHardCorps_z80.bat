@echo off

cls
bin\asw  -xx -c -A SuperHardCorps\Z80\image.asm
IF NOT EXIST SuperHardCorps\Z80\image.p goto LABLPAUSE
IF EXIST SuperHardCorps\Z80\image.p goto Convert2ROM
:Convert2ROM
bin\p2bin SuperHardCorps\Z80\image.p SuperHardCorps\Z80\z80image_build.bin -l 0 -r $-$
REM -r 0x-0x
del SuperHardCorps\Z80\image.p
del SuperHardCorps\Z80\image.h
exit /b
:LABLPAUSE

pause


exit /b