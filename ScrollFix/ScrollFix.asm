; ---------------------------------------------------------------------------
; Contra Hard Corps - Sega Gopher Scroll Fix
;
; Author: Mikhail 'Segaman' Shilenko
; Created 02.07.2012
; ---------------------------------------------------------------------------

; ---------------------------------------------------------------------------
; Insert Configuration
; ---------------------------------------------------------------------------
	include ScrollFix\Config.asm

; ---------------------------------------------------------------------------
; Insert ROM Image
; ---------------------------------------------------------------------------
	org	$0
	incbin	"Contra - Hard Corps (U).bin"
	
; ---------------------------------------------------------------------------
; Settings
; ---------------------------------------------------------------------------
scroll_fix = 1
scroll_bug = 0

; ---------------------------------------------------------------------------
; Header
; ---------------------------------------------------------------------------
	org $120
	dc.b "CONTRA HARDCORPS - ELEKTROPAGE.RU SCROLL FIX    "
	dc.b "CONTRA HARDCORPS - ELEKTROPAGE.RU SCROLL FIX    "
	
; ---------------------------------------------------------------------------
; Skip checksum
; ---------------------------------------------------------------------------
	org  $384
	jmp  $39E
; ---------------------------------------------------------------------------
; ScrollBug
; ---------------------------------------------------------------------------
if scroll_bug

	org     $17636
	jmp     skip
	
endif
        
        ;include HitPoint.asm

;Scroll Fix
        if scroll_fix
        ;Vsync
        ;org     $3FE
        ;jsr     (SetVsync).l
        org     $78A4  ; Lev1
        jmp     DMA_plus_1
	org     $7AD8  ; Lev2
        jmp     DMA_plus_2
	org     $167F0 ; Lev3
        jmp     DMA_plus_3
	;Lev1
        org     $1762E
	jmp     SF_L1_p0
        org     $17776
	jmp     SF_L1_p1
        
        ;Lev 2_1
	org     $179F8
	jmp     SF_L2_1_p0
	org     $17A7E
        jmp     SF_L2_1_p1
        org     $17BD8
        jmp     SF_L2_1_p2

        ;Lev 2_2
	org     $17D08
	jmp     SF_L2_2_p0

        ;Lev 3
	org     $1656C
	jmp     SF_L3_p0
	org     $165C4
	jmp     SF_L3_p1
	org     $16630
        jmp     SF_L3_p2
	org     $166B6
	jmp     SF_L3_p3
	
        ;Lev 7
	org     $200E4
	jmp     SF_L7_p0
	org     $200FE
	jmp     SF_L7_p1
        org     $20172
        lea     (Lev7ScrollList).l,a3
	lea     ($FFFFE9A0).w,a4
	org     $201EE
	lea     ($FFFFE9A0).w,a6
	lea     (Lev7ScrollSize).l,a3
        lea     (Lev7ScrollList).l,a4
        org     $20236
	jmp     SF_L7_p2
	org     $20250
	jmp     SF_L7_p3
        org     $202DC
	lea     ($FFFFE600).w,a0
	org     $7940
	lea     ($FFFFE780).w,a1
	org     $20314
	lea     ($FFFFE780).w,a0
        org     $20358
        move.l  #$FFFFE600,d6

        ;Lev 8
	org     $206A8
	jmp     SF_L8_p0
	org     $20716
	dc.b   0,  6,$FF,$FF,$EC,$18; 12
	dc.b   0,$3E,$FF,$FF,$EC,$14; 18
	org     $207B8
	dc.b   0,  6,$FF,$FF,$EC,$18; 12
	dc.b   0,$3E,$FF,$FF,$EC,$14; 18
        
        ;Lev 11
	org     $1880E
	jmp     SF_L11_p0
	;clr.w   (a2)
        ;rts
	endif


	org     $AF000
        if scroll_bug
skip:
        clr.l   (a2)+
        clr.l   (a2)+
        clr.l   (a2)+
        jmp     $17648
	endif
        if scroll_fix
Load:
		move.w	d4,(a0)+
		addq.l  #2,a0
		dbf	d5,Load
		rts
LoadX:
		move.w	d4,(a0)+
		dbf	d5,LoadX
		rts
LoadY:
		move.w	(a1)+,(a0)+
		dbf	d5,LoadY
		rts
Load2:
		move.l	(a1)+,(a0)+
		dbf	d5,Load2
		rts

DMA_plus_1:
                cmpi.w  #3,d0
                beq.s   loc_0_78A4_plus
                move.w  #$8F04,(a0)
                bra.s   loc_0_78A4
loc_0_78A4_plus:                             ; CODE XREF: sub_0_7858+42j
                move.w  #$8F02,(a0)
loc_0_78A4:                             ; CODE XREF: sub_0_7858+42j
                move.w  d4,(a0)
                move.w  #$100,($A11100).l
                move.l  (a2)+,(a0)
                move.l  (a2)+,(a0)
                move.w  (a2)+,(a0)
                move.l  (a2),d2
                move.w  (a2)+,(a0)
                move.w  (a2)+,-(sp)

loc_0_78BE:                             ; CODE XREF: sub_0_7858+6Ej
                btst    #0,($A11100).l
                bne.s   loc_0_78BE
                move.w  (sp)+,(a0)
                move.w  #0,($A11100).l
                move.w  d7,(a0)
                andi.w  #3,d2
                movea.l (a2)+,a3
                move.l  d2,(a0)
                move.w  (a3),-4(a0)
                jmp     $7892


DMA_plus_2:
                cmpi.w  #3,d0
                beq.s   loc_0_7AD8_plus
                move.w  #$8F20,(a0)
                bra.s   loc_0_7AD8
loc_0_7AD8_plus:                             ; CODE XREF: ROM:00007ACAj
                move.w  #$8F04,(a0)
loc_0_7AD8:                             ; CODE XREF: ROM:00007ACAj
                move.w  d4,(a0)
                move.w  #$100,($A11100).l
                move.l  (a2)+,(a0)
                move.l  (a2)+,(a0)
                move.w  (a2)+,(a0)
                move.l  (a2),d2
                move.w  (a2)+,(a0)
                move.w  (a2)+,-(sp)

loc_1_7AF2:                             ; CODE XREF: ROM:00007AFAj
                btst    #0,($A11100).l
                bne.s   loc_1_7AF2
                move.w  (sp)+,(a0)
                move.w  #0,($A11100).l
                move.w  d7,(a0)
                andi.w  #3,d2
                movea.l (a2)+,a3
                move.l  d2,(a0)
                move.w  (a3),-4(a0)
                jmp     $7AC2


DMA_plus_3:
                cmpi.w  #3,d0
                beq.s   loc_0_167F0_plus
                move.w  #$8F04,(a0)
                bra.s   loc_0_167F0
loc_0_167F0_plus:
                move.w  #$8F02,(a0)
loc_0_167F0:                            ; CODE XREF: ROM:000167E2j
                move.w  d4,(a0)
                move.w  #$100,($A11100).l
                move.l  (a2)+,(a0)
                move.l  (a2)+,(a0)
                move.w  (a2)+,(a0)
                move.l  (a2),d2
                move.w  (a2)+,(a0)
                move.w  (a2)+,-(sp)

loc_0_1680A:                            ; CODE XREF: ROM:00016812j
                btst    #0,($A11100).l
                bne.s   loc_0_1680A
                move.w  (sp)+,(a0)
                move.w  #0,($A11100).l
                move.w  d7,(a0)
                andi.w  #3,d2
                movea.l (a2)+,a3
                move.l  d2,(a0)
                move.w  (a3),-4(a0)
                jmp     $167DA
; ---------------------------------------------------------------------------

set_DMA3:                               ; CODE XREF: ROM:0001766Cp
                                        ; ROM:0001779Ap ...
                move.w  #3,(a2)+
                lsl.l   #8,d5
                lsr.w   #8,d5
                andi.l  #$FF00FF,d5
                ori.l   #$94009300,d5
                move.l  d5,(a2)+
                move.l  d6,d4
                move.l  d6,d5
                lsl.l   #7,d6
                lsr.w   #8,d6
                andi.l  #$FF00FF,d6
                ori.l   #$96009500,d6
                move.l  d6,(a2)+
                swap    d5
                lsr.w   #1,d5
                andi.w  #$7F,d5 ; ''
                ori.w   #$9700,d5
                move.w  d5,(a2)+
                move.w  d7,d6
                andi.w  #$3FFF,d6

loc_0_7810:
                ori.w   #$4000,d6

loc_0_7814:
                move.w  d6,(a2)+
                rol.w   #2,d7
                andi.w  #3,d7
                ori.w   #$80,d7 ; '�'
                move.w  d7,(a2)+
                move.l  d4,(a2)+
                rts
; End of function set_DMA3
set_DMA4:                                ; CODE XREF: ROM:000165A4p
                                        ; ROM:000165BAp ...
                move.w  #4,(a2)+
                lsl.l   #8,d5
                lsr.w   #8,d5
                andi.l  #$FF00FF,d5
                ori.l   #$94009300,d5
                move.l  d5,(a2)+
                move.w  d7,d6
                andi.w  #$3FFF,d6
                ori.w   #$4000,d6
                move.w  d6,(a2)+
                rol.w   #2,d7
                andi.w  #3,d7
                ori.w   #$80,d7 ; '�'
                move.w  d7,(a2)+
                move.w  d4,(a2)+
                rts
; End of function set_DMA

        include  ScrollFix\Lev1.asm
	include  ScrollFix\Lev2_1.asm
	include  ScrollFix\Lev2_2.asm
	include  ScrollFix\Lev3.asm
	include  ScrollFix\Lev7.asm
        include  ScrollFix\Lev8.asm
        include  ScrollFix\Lev11.asm

	endif

