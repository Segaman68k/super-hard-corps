; 200E4 = train
; 200FE = rails + train
Lev7ScrollSize  equ $2026A

SF_L7_p0:
                lea     ($FFFFE600).w,a0
                move.w  d5,-(sp)
                sub.w   #1,d5
                jsr     (LoadX).l
                move.w  (sp)+,d5
                move.l  #$FFFFE600,d6
                jsr     (set_DMA2).l
                jmp     $200EA
SF_L7_p1:
                lea     ($FFFFE5C0).w,a0
                move.w  d5,-(sp)
                sub.w   #1,d5
                jsr     (LoadX).l
                move.w  (sp)+,d5
                move.l  #$FFFFE5C0,d6
                jsr     (set_DMA2).l
                lea     ($FFFFE954).w,a0
                jmp     $20108
SF_L7_p2:
                ;lea     ($FFFF3440).l,a0
                move.l  a0,-(sp)
                move.w  d5,-(sp)
                sub.w   #1,d5
                add.l   #$3386,a0
                jsr     (LoadX).l
                move.w  (sp)+,d5
                move.l  (sp)+,a0
                move.l  #$FFFF3386,d6
                add.l   d7,d6
                jsr     (set_DMA2).l
                jmp     $2023C
SF_L7_p3:
                ;lea     ($FFFF3440).l,a0
                move.l  a0,-(sp)
                move.w  d5,-(sp)
                sub.w   #1,d5
                add.l   #$3386,a0
                jsr     (LoadX).l
                move.w  (sp)+,d5
                move.l  (sp)+,a0
                move.l  #$FFFF3386,d6
                add.l   d7,d6
                jsr     (set_DMA2).l
                jmp     $20256
; ---------------------------------------------------------------------------
Lev7ScrollList: dc.l $FFFFE994          ; 0 ; DATA XREF: ROM:00020172o
                                        ; ROM:000201F8o
                dc.l $FFFFE998          ; 1
                dc.l $FFFFE99C          ; 2
                dc.l $FFFFE954          ; 3
                dc.l $FFFFE958          ; 4
                dc.l $FFFFE95C          ; 5
                dc.l $FFFFE960          ; 6
                dc.l $FFFFE964          ; 7
                dc.l $FFFFE968          ; 8
                dc.l $FFFFE96C          ; 9
                dc.l $FFFFE970          ; 10
                dc.l $FFFFE974          ; 11
                dc.l $FFFFE978          ; 12
                dc.l $FFFFE97C          ; 13
                dc.l $FFFFE980          ; 14
                dc.l $FFFFE984          ; 15
                dc.l $FFFFE988          ; 16
                dc.l $FFFFE98C          ; 17
                dc.l $FFFFE990          ; 18
; ---------------------------------------------------------------------------
