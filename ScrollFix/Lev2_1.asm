;=====================================================================================================
SF_L2_1_p0:
                move.l  ($FFFFA0F0).w,d0
                asr.l   #3,d0
                move.l  d0,($FFFFA100).w
                move.l  ($FFFFA0F8).w,d0
                neg.l   d0
                move.l  d0,d1
                swap    d1
                move.w  d1,($FFFFE7FA).w
                asr.l   #2,d0
                swap    d0
                move.w  d0,($FFFFE7F8).w
                lea     ($FFFFE7A0).w,a2
                move.l  a2,($FFFFA1EE).w

                clr.l   d5
                move.w  ($FFFFE800).w,($FFFFE7F0).w
                move.w  ($FFFFE7F0).w,d4
                add.l   #$20,d4
                move.w  #$FE,d5
                lea     ($FFFFE800).w,a0;($FFFFB8A0).w,a0
                clr.w   (a0)+
		jsr     (LoadX).l
                move.w  ($FFFFE7F8).w,d4
                moveq   #$50,d5
                lea     ($FFFFEA02).w,a0;($FFFFB8A2).w,a0
		jsr     (LoadX).l
                move.w  ($FFFFE7FA).w,d4
                move.w   #$96,d5
                lea     ($FFFFEA82).w,a0;($FFFFB9A2).w,a0
		jsr     (LoadX).l
                move.w  #$B800,d7
                move.w  #$E0,d5 ; '�'
                move.l  #$FFFFE800,d6
                jsr     (set_DMA3).l
                move.w  #$B882,d7
                move.w  #$E0,d5 ; '�'
                move.l  #$FFFFEA00,d6
                jsr     (set_DMA3).l
                ;jsr     (LoadScrollTable2).l;move.l  #$1,($FFFFFFF0).w

                clr.w   (a2)
                rts
;=====================================================================================================
SF_L2_1_p1:
                move.l  ($FFFFA0F0).w,d0
                asr.l   #3,d0
                move.l  d0,($FFFFA100).w
                move.l  ($FFFFA0F8).w,d0
                neg.l   d0
                move.l  d0,d1
                swap    d1
                move.w  d1,($FFFFE7FA).w
                asr.l   #2,d0
                swap    d0
                move.w  d0,($FFFFE7F8).w
                move.w  ($FFFFE7FA).w,d0
                add.w   d0,d0
                move.w  d0,($FFFFE7FC).w
                move.l  ($FFFFE800).w,($FFFFE7F0).w
                lea     ($FFFFE7A0).w,a2
                move.l  a2,($FFFFA1EE).w
                move.w  ($FFFFE7F2).w,d4
                move.w  #$80,d5
                lea     ($FFFFE800).w,a0;#$FFFFB8A0,d7
                clr.w   (a0)+
		jsr     (LoadX).l
                move.w  ($FFFFE7F0).w,d4
                move.w  #$F0,d5
                lea     ($FFFFE8D0).w,a0;#$FFFFB9A0,d7
                jsr     (LoadX).l
                move.w  ($FFFFE7F8).w,d4
                move.w  #$80,d5
                lea     ($FFFFEA00).w,a0;#$FFFFB8A2,d7
                jsr     (LoadX).l
                move.w  ($FFFFE7FA).w,d4
                move.w  #$E0,d5
                lea     ($FFFFEA82).w,a0;#$FFFFB9A2,d7
                jsr     (LoadX).l
                move.w  ($FFFFE7FC).w,d4
                moveq   #10,d5
                lea     ($FFFFEB70).w,a0;#$FFFFBB62,d7
                jsr     (LoadX).l
                move.w  #$B800,d7
                move.w  #$E0,d5 ; '�'
                move.l  #$FFFFE800,d6
                jsr     (set_DMA3).l
                move.w  #$B882,d7
                move.w  #$E0,d5 ; '�'
                move.l  #$FFFFEA00,d6
                jsr     (set_DMA3).l
                ;jsr     (LoadScrollTable2).l;move.l  #$1,($FFFFFFF0).w
                clr.w   (a2)
                rts
;=====================================================================================================
SF_L2_1_p2:
                lea     ($FFFFE7A0).w,a2
                move.l  a2,($FFFFA1EE).w
                ;move.w  ($FFFFE800).w,d4
                ;move.w  #$B8,d5 ; '�'
                ;move.w  #$B8A0,d7
                move.w  ($FFFFE800).w,d4
                move.w  #$FE,d5 ; '�'
                lea     ($FFFFE800).w,a0;#$B8A0,d7
                clr.w   (a0)+
		jsr     (LoadX).l
                ;jsr     (LoadScrollTable2).l;jsr     (set_DMA).l
                move.w  #$B800,d7
                move.w  #$E0,d5 ; '�'
                move.l  #$FFFFE800,d6
                jsr     (set_DMA2).l
                move.l  #$FFFFEC50,d6
                move.w  #$B8,d5 ; '�'
                move.w  #$B8A2,d7
                jsr     (set_DMA2).l
                ;move.l  #$1,($FFFFFFF0).w
                clr.w   (a2)
                rts

