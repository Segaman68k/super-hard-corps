SF_L8_p0:
;                lea     ($2072A).l,a0

loc_0_206A8:                            ; CODE XREF: ROM:00020668j
                lea     ($FFFFE7C0).w,a2
                move.l  a2,($FFFFA1EE).w
                movem.l  a0-a1,-(sp)
                move.l  a0,a1
                move.w  ($FFFFE800).w,d4
                move.w  #$EF,d5 ; '`'
                lea     ($FFFFE600).w,a0;move.w  #$BA00,d7
                jsr     (LoadX).l
                move.w  #$B880,d7
                move.w  #$EF,d5 ; '�'
                move.l  #$FFFFE600,d6
                jsr     (set_DMA2).l
                move.w  #$B882,d7
                move.w  #$EF,d5 ; '�'
                move.l  #$FFFFE900,d6
                jsr     (set_DMA2).l
                lea     ($FFFFE900).w,a0;move.w  #$BA00,d7
                
loc_0_206C6:                            ; CODE XREF: ROM:000206EEj
                                        ; ROM:00020708j
                cmpi.w  #-1,(a1)
                bne.s   loc_0_206D0
                clr.w   (a2)
                movem.l  (sp)+,a0-a1
                rts
; ---------------------------------------------------------------------------

loc_0_206D0:                            ; CODE XREF: ROM:000206CAj
                tst.b   (a1)+
                bne.s   loc_0_206F0
                move.b  (a1)+,d5
                andi.w  #$FF,d5
                movea.l (a1)+,a3
                move.w  (a3),d4
                move.w  d0,d7
                move.w  d5,d1
                add.w   d1,d1
                add.w   d1,d1
                add.w   d1,d0
                jsr     (LoadX).l;(set_DMA).l
                bra.s   loc_0_206C6
; ---------------------------------------------------------------------------

loc_0_206F0:                            ; CODE XREF: ROM:000206D2j
                move.b  (a1)+,d5
                andi.w  #$FF,d5
                move.l  a1,-(sp)
                move.l  (a1),a1
                move.w  d0,d7
                move.w  d5,d1
                add.w   d1,d1
                add.w   d1,d1
                add.w   d1,d0
                jsr     (LoadY).l;(set_DMA2).l
                add.l   #4,(sp)
                move.l  (sp)+,a1
                bra.s   loc_0_206C6