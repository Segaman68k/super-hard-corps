SF_L3_p0:
                lea     ($FFFFEA14).w,a2
                move.w  ($FFFFA0EC).w,d1
                add.w   ($FFFFA1DC).w,d1
                move.w  ($FFFFE800).w,d4
                move.w  #$EF,d5 ; '`'
                add.w   d1,d5
                lea     ($FFFFE600).w,a0;move.w  #$BA00,d7
                jsr     (LoadX).l
                move.w  ($FFFFEA04).w,d4
                move.w  #$EF,d5 ; '�'
                lea     ($FFFFE800).w,a0;move.w  #$B982,d7
                jsr     (LoadX).l
                ;move.w  ($FFFFEA0C).w,d4
                ;move.w  #$3C,d5 ; '<'
                ;lea     ($FFFFE960).w,a0;move.w  #$BA92,d7
                ;jsr     (LoadX).l
                move.w  #$B800,d7
                move.w  #$EF,d5 ; '�'
                move.l  #$FFFFE600,d6
                jsr     (set_DMA2).l
                move.w  #$B802,d7
                move.w  #$E0,d5 ; '�'
                move.l  #$FFFFE800,d6
                jsr     (set_DMA2).l
                move.w  d1,d2
                add.w   d2,d2
                add.w   d2,d2
                move.w  #$B860,d7
                sub.w   d2,d7
                move.w  #$6F,d5 ; '�'
                move.l  #$FFFFEC30,d6
                jsr     (set_DMA2).l
                clr.w   (a2)
                rts
; ---------------------------------------------------------------------------
SF_L3_p1:
                lea     ($FFFFEA14).w,a2
                move.w  ($FFFFA0EC).w,d1
                add.w   ($FFFFA1DC).w,d1
                move.w  ($FFFFE800).w,d4
                move.w  #$EF,d5 ; '`'
                add.w   d1,d5
                lea     ($FFFFE600).w,a0;move.w  #$BA00,d7
                jsr     (LoadX).l
                move.w  ($FFFFEA04).w,d4
                move.w  #$EF,d5 ; '�'
                lea     ($FFFFE800).w,a0;move.w  #$B982,d7
                jsr     (LoadX).l
                move.w  ($FFFFEA0C).w,d4
                move.w  #$3C,d5 ; '<'
                lea     ($FFFFE960).w,a0;move.w  #$BA92,d7
                jsr     (LoadX).l
                move.w  #$B800,d7
                move.w  #$EF,d5 ; '�'
                move.l  #$FFFFE600,d6
                jsr     (set_DMA2).l
                move.w  #$B802,d7
                move.w  #$E0,d5 ; '�'
                move.l  #$FFFFE800,d6
                jsr     (set_DMA2).l
                move.w  d1,d2
                add.w   d2,d2
                add.w   d2,d2
                move.w  #$B860,d7
                sub.w   d2,d7
                move.w  #$6F,d5 ; '�'
                move.l  #$FFFFEC30,d6
                jsr     (set_DMA2).l
                clr.w   (a2)
                rts
; ---------------------------------------------------------------------------
SF_L3_p2:
                lea     ($FFFFEA14).w,a2
                move.w  ($FFFFA1DC).w,d1
                move.w  ($FFFFE800).w,d4
                move.w  #$EF,d5 ; '`'
                add.w   d1,d5
                lea     ($FFFFE600).w,a0;move.w  #$BA00,d7
                jsr     (LoadX).l
                move.w  ($FFFFEA04).w,d4
                move.w  #$EF,d5 ; '�'
                lea     ($FFFFE800).w,a0;move.w  #$B982,d7
                jsr     (LoadX).l
                move.w  ($FFFFEA0C).w,d4
                move.w  #$3C,d5 ; '<'
                lea     ($FFFFE960).w,a0;move.w  #$BA92,d7
                jsr     (LoadX).l
                move.w  #$B800,d7
                move.w  #$EF,d5 ; '�'
                move.l  #$FFFFE600,d6
                jsr     (set_DMA2).l
                move.w  #$B802,d7
                move.w  #$E0,d5 ; '�'
                move.l  #$FFFFE800,d6
                jsr     (set_DMA2).l
                move.w  d1,d2
                add.w   d2,d2
                add.w   d2,d2
                move.w  #$B860,d7
                sub.w   d2,d7
                move.w  #$6F,d5 ; '�'
                move.l  #$FFFFEC30,d6
                jsr     (set_DMA2).l
                move.w  #$B862,d7
                sub.w   d2,d7
                move.w  #$5F,d5 ; 'x'
                move.l  #$FFFFEC30,d6
                jsr     (set_DMA2).l

                clr.w   (a2)
                rts
; ---------------------------------------------------------------------------
SF_L3_p3:
                lea     ($FFFFEA14).w,a2
                move.w  ($FFFFA0EC).w,d1
                add.w   ($FFFFA1DC).w,d1
                move.w  #$7C,d5 ; '|'
                sub.w   d1,d5
                ble.s   loc_0_166F2
                move.w  #$B8A2,d7
                move.w  d1,d6
                addi.w  #$28,d6 ; '('
                ext.l   d6
                add.l   d6,d6
                addi.l  #$FFFFEC00,d6
                jsr     (set_DMA2).l
                move.w  ($FFFFE800).w,d4
                move.w  #$EF,d5 ; '`'
                lea     ($FFFFE600).w,a0
                jsr     (LoadX).l
                move.l  #$FFFFE600,d6
                move.w  #$B8A0,d7
                move.w  #$B8,d5 ; '�'
                jsr     (set_DMA2).l

loc_0_166F2:                            ; CODE XREF: ROM:000166C4j
                move.w  ($FFFFEA0C).w,d4
                move.w  #$EF,d5 ; '`'
                lea     ($FFFFE800).w,a0
                jsr     (LoadX).l
                move.l  #$FFFFE800,d6
                cmpi.w  #$7C,d1 ; '|'
                bge.s   loc_0_16712
                move.w  #$A4,d7 ; '�'
                sub.w   d1,d7
                add.w   d7,d7
                add.w   d7,d7
                addi.w  #$B802,d7
                move.w  #$3C,d5 ; '<'
                add.w   d1,d5
                bra.s   loc_0_1671A
; ---------------------------------------------------------------------------

loc_0_16712:                            ; CODE XREF: ROM:000166FAj
                move.w  #$B8A2,d7
                move.w  #$B8,d5 ; '�'

loc_0_1671A:                            ; CODE XREF: ROM:00016710j
                jsr     (set_DMA2).l
                clr.w   (a2)
                rts
