
onScreenIDs		= 0
onScreenIDlist	= 1
overList		= 0

function turnOnScreenIDs()
	if( onScreenIDs == 0 ) then
		onScreenIDs = 1
		return
	end
	if( onScreenIDs == 1 ) then
		onScreenIDs = 0
		return
	end
end

function turnOnScreenIDlist()
	if( onScreenIDlist == 0 ) then
		onScreenIDlist = 1
		return
	end
	
	if( onScreenIDlist == 1 ) then
		onScreenIDlist = 0
		return
	end
end


function showObjectData(address,index,xx)
	local ID
	local x
	local y
	local h
	local v
	local text
	
	if( overList == 1 ) then
		return
	end
	
	ID		= memory.readword(address)
	x		= memory.readword(address + 0x06)
	y		= memory.readword(address + 0x0A)
	h		= memory.readword(address + 0x0E)
	v		= memory.readword(address + 0x12)
	text	= string.format("%04X:ID%04X,x%04X,y%04X,h%04X,v%04X", AND(address,0xFFFF), ID, x, y, h, v )
	--print ( text )
	if( onScreenIDlist == 1 ) then
		gui.drawtext (4 + xx*160, 8 + index*8 , text, 0xFFFFFFA0, 0x000000A0 )
	end
	
	if( onScreenIDs == 1 ) then
		if( x > 0x0080 ) then
			if( x < 0x01C0 ) then
				local ox
				local oy
				
				ox		= x - 0x80
				oy		= y - 0x88
				
				--print( string.format("%04X:%04X,%04X",AND(address,0xFFFF),ox,oy) )
				
				text	= string.format("%04X:ID%04X", AND(address,0xFFFF), ID, x, y )
				gui.drawtext ( ox, oy , text, 0xFFFFFF80, 0x00000080 )
			end
		end
	end
end

gens.registerafter( function()
	local i
	local ii
	local iii
	local x
	local last
	local step
	local id
	local line
	
	i		= 0xFFB000
	ii		= 0
	iii		= 0
	x		= 0
	last	= 0xFFD800
	step	= 0x80
	line	= 0xC00
    
	while i < last do
		id	= memory.readword( i )
		if( id ~= 0 ) then
			local addr
			local ind
			local listi
			
			addr	= i
			ind		= ii
			listi	= x
			
			ii	= ii+1
			iii = iii+step
			if( iii == line ) then
				ii = 0
				iii = 0
				x = x+1
				
			end
			
			if( x > 1 ) then
				overList = 1
			else
				overList = 0
			end
			
			if( overList == 0 ) then
				showObjectData( addr, ind, listi )
			else
				if( onScreenIDlist == 1 ) then
					gui.drawtext ( 0, 0 , "overspaced", 0xFFFFFFF0, 0x7F000080 )
					i = last
				end
			end
		end
		i = i+step
	end
end)

input.registerhotkey( 1, function() turnOnScreenIDs() end )
input.registerhotkey( 2, function() turnOnScreenIDlist() end )

