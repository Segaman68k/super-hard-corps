;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
; $1C(OBJ) == %0000 0100 = Boss taking damage
; $26(OBJ).l = Animation Address
; $36(OBJ) - Boss HP
;
; $0392B0 PC - Taking damage
; $0392e2:4e75 - Making another behavior
;
; $2BCB8 PC - ������ ��������� ������������ �����
; ������ ������ (������� � ����������)
; $2B2F0 PC - ������ ������ ������ ������ (�� ����������)
; $2AF34 Pc - ������ �������� �� ������ ������ ������
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0
 
Obj009C_FullHP		equ $BE0
Obj009C_FirstHP		equ $420
Obj009C_HorSpeed	equ $00080000;$00060000
Obj009C_HorFriction	equ $0000C000;$00008000

; ---------------------------------------------------------------------------
; ��������� ����������� �������� ��������� �� 4 ����������
; ---------------------------------------------------------------------------
	org		$2B29A
	move.b	#$03,$6E(a0)
	bra.s	$2B2A4
	
; ---------------------------------------------------------------------------
; ������ ����� ��������
; ---------------------------------------------------------------------------
	org		$2AEE0
	bra.s	$2AEE6
	
; ---------------------------------------------------------------------------
; ��������� ����� ������ �� ����� ����� ��������
; ---------------------------------------------------------------------------
	org		$2AEAE
	cmpi.b	#$30,$62(a0)
	
	org		$2AEB6
	cmpi.b	#$10,$62(a0)
	
; ---------------------------------------------------------------------------
; ���������� �������� ����� ����� ��������� ����� ������
; ---------------------------------------------------------------------------
	org		$2B52C
	move.b	#$1,$62(a0)
	
; ---------------------------------------------------------------------------
; ��������� �������� HP (#$0960)
; ---------------------------------------------------------------------------
	org		$2ABCA
	move.w	#Obj009C_FullHP,$36(a0)
	
; ---------------------------------------------------------------------------
; ��������� HP ������ ������ (#$04B0)
; ---------------------------------------------------------------------------
	org		$2BCB0
	cmpi.w	#Obj009C_FirstHP,$36(a0)
	
; ---------------------------------------------------------------------------
; ��������� �������������� �������� ����
; ---------------------------------------------------------------------------
	org		$2BA52
	cmpi.l	#Obj009C_HorSpeed,$E(a0)
	
	org		$2BA3E
	cmpi.l	#-Obj009C_HorSpeed,$E(a0)
	
; ---------------------------------------------------------------------------
; ��������� �������������� �������� ������ ������� ����� ����
; ---------------------------------------------------------------------------
	org		$2B72A
	move.l	#-Obj009C_HorSpeed,$E(a4)
	
	;org		$2B7CA
	;move.l	#-Obj009C_HorSpeed,$E(a4)
	
	org		$2B698
	subi.l	#$00009000,$E(a4)
	
; ---------------------------------------------------------------------------
; ��������� �������� ������ ������ ������ (��� ������)
; ---------------------------------------------------------------------------
	org		$2BCB8
	jmp		Obj009C_CustomDeath
	;move.b	#$02,$77(a0)
	;move.b	#$05,$68(a0)
	
; ---------------------------------------------------------------------------
; ���� � �������
; ---------------------------------------------------------------------------
	org		$3659E
	dc.l	Obj009C_BulletHellSit
	
	org		$365A6
	dc.l	Obj009C_BulletHellSit2
	
; ---------------------------------------------------------------------------
; ���� ����
; ---------------------------------------------------------------------------
	org		$3659A
	dc.l	Obj009C_BulletHellStand
	
	org		$365A2
	dc.l	Obj009C_BulletHellStand2
	
; ---------------------------------------------------------------------------
; �������� ����������� � ���������
; ---------------------------------------------------------------------------
	org		$2B2C6
	jmp		Obj009C_RNGtoBeh76

	;org		$2AFB2
	;jmp		Obj009C_RNGtoBeh77
	
; ---------------------------------------------------------------------------
; ��������� ������������� ��� ����������
; ---------------------------------------------------------------------------
	org		$2BA10
	add.l	#Obj009C_HorFriction,$E(a0)
	
	org		$2BA24
	sub.l	#Obj009C_HorFriction,$E(a0)
	
; ---------------------------------------------------------------------------
; ��������� �������� �������� ������������ ����
; ---------------------------------------------------------------------------
	;org		$2B5C4
	;nop
	;nop
	
	;org		$2BAD4
	;jmp		Obj009C_FlyingFix
	
; ---------------------------------------------------------------------------
; ��������
; ---------------------------------------------------------------------------
	; �������� �������� � �������
	org		$28B02
	
	dc.w $0002,$0008,$4442
	dc.w $0048,$0008,$44A4
	dc.w $0004,$0008,$4506
	dc.w $0001,$0008,$44A4
	dc.w $0001,$0008,$4442
	dc.w $0001,$0008,$43D8
	dc.w $0001,$0008,$436E
	dc.w $0000,$0008,$3BFA
	
	; �������� �������� ����
	org		$28AE4
	
	dc.w $0002,$0008,$3BFA
	dc.w $0048,$0008,$3C64
	dc.w $0002,$0008,$3CCE
	dc.w $0001,$0008,$3C64
	dc.w $0000,$0008,$3BFA
	
	; �������� ���������� � ��������
	org		$289FA
	
	dc.w $0001,$0008,$436E
	dc.w $0001,$0008,$4442
	dc.w $0000,$0008,$43D8
	
	; �������� ����������� � ���� ������
	org		$28C82
	
	dc.w $0002,$0008,$3BFA
	dc.w $0002,$0008,$436E
	dc.w $0002,$0008,$43D8
	dc.w $0002,$0008,$436E
	dc.w $0004,$0008,$3BFA
	dc.w $0004,$0008,$4B88
	dc.w $0000,$0008,$4BFA
	
	; �������� ����������� � ������
	org		$28CAC
	
	dc.w $0002,$0008,$38BE
	dc.w $0002,$0008,$3948
	dc.w $0002,$0008,$39D2
	dc.w $0002,$0008,$3A5C
	dc.w $0004,$0008,$39D2
	dc.w $0004,$0008,$3948
	dc.w $0000,$0008,$38BE
	
	; �������� ��������� ��� ������������ ������� ����� ����
	org		$28BA4

	dc.w $0002,$0008,$594A
	dc.w $0002,$0008,$59BC
	dc.w $0001,$0008,$5A36
	dc.w $0001,$0008,$5AB0
	dc.w $0000,$0008,$5AB0
	
	; �������� ������������� ������� ����� ����
	org		$28C64

	dc.w $0002,$0008,$5AB0
	dc.w $0002,$0008,$5A36
	dc.w $0001,$0008,$59BC
	dc.w $0001,$0008,$594A
	dc.w $0000,$0008,$549A
	
	; �������� ������� ��� ����� � ����
	org		$28BF8

	dc.w $0002,$0008,$5C76
	dc.w $0002,$0008,$5CE0
	dc.w $0002,$0008,$5D4A
	dc.w $0000,$0008,$5D4A
	
	; �������� ����� � ����
	;org		$28C10

	;dc.w $0002,$0008,$5D4A
	;dc.w $0002,$0008,$5DB4
	;dc.w $0002,$0008,$5E16
	;dc.w $0002,$0008,$5E68
	;dc.w $0002,$0008,$5EC2
	;dc.w $0000,$0008,$5EC2
	
	org		$2B982
	move.l	#Obj009C_CustomRunningAnim,$26(a0)
	
	; �������� ��������� ����� ����
	org		$28C34

	dc.w $0003,$0008,$5EC2
	dc.w $0003,$0008,$5F34
	dc.w $0002,$0008,$5F8E
	dc.w $0000,$0008,$5FE8
	;dc.w $0002,$0008,$6032
	;dc.w $0003,$0008,$6094
	;dc.w $0003,$0008,$6032
	;dc.w $0000,$0008,$5FE8

 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1
 
Obj009C_CustomDeath:
	move.b	#$02,$77(a0)
	move.b	#$05,$68(a0)
	move.w	#$0400,$42(a0)
	jmp		$2BCC4

Obj009C_BulletHellSit:
	dc.w $0000,$00CD,$0030,$FFDD
	dc.w $0800,$00CD,$0030,$FFDD
	dc.w $1000,$00CD,$0030,$FFDD
	dc.w $1800,$00CD,$0030,$FFDD
	dc.w $2000,$00CD,$0030,$FFDD
	dc.w $2800,$00CD,$0030,$FFDD
	dc.w $3000,$00CD,$0030,$FFDD
	dc.w $3800,$00CD,$0030,$FFDD
	dc.w $4000,$00CD,$0030,$FFDD
	dc.w $4800,$00CD,$0030,$FFDD
	dc.w $5000,$00CD,$0030,$FFDD
	dc.w $5800,$00CD,$0030,$FFDD
	dc.w $FFFF
	
Obj009C_BulletHellSit2:
	dc.w $0000,$00CE,$0014,$FFDD
	dc.w $0800,$00CE,$0014,$FFDD
	dc.w $1000,$00CE,$0014,$FFDD
	dc.w $1800,$00CE,$0014,$FFDD
	dc.w $2000,$00CE,$0014,$FFDD
	dc.w $2800,$00CE,$0014,$FFDD
	dc.w $3000,$00CE,$0014,$FFDD
	dc.w $3800,$00CE,$0014,$FFDD
	dc.w $4000,$00CE,$0014,$FFDD
	dc.w $4800,$00CE,$0014,$FFDD
	dc.w $5000,$00CE,$0014,$FFDD
	dc.w $5800,$00CE,$0014,$FFDD
	dc.w $FFFF

Obj009C_BulletHellStand: ; $37822
	dc.w $0000,$00CD,$0030,$FFC4
	dc.w $0800,$00CD,$0030,$FFC4
	dc.w $1000,$00CD,$0030,$FFC4
	dc.w $1800,$00CD,$0030,$FFC4
	dc.w $2000,$00CD,$0030,$FFC4
	dc.w $2800,$00CD,$0030,$FFC4
	dc.w $3000,$00CD,$0030,$FFC4
	dc.w $3800,$00CD,$0030,$FFC4
	dc.w $FFFF
	
Obj009C_BulletHellStand2: ; $37886
	dc.w $0000,$00CE,$0014,$FFC8
	dc.w $0800,$00CE,$0014,$FFC8
	dc.w $1000,$00CE,$0014,$FFC8
	dc.w $1800,$00CE,$0014,$FFC8
	dc.w $2000,$00CE,$0014,$FFC8
	dc.w $2800,$00CE,$0014,$FFC8
	dc.w $3000,$00CE,$0014,$FFC8
	dc.w $3800,$00CE,$0014,$FFC8
	dc.w $FFFF
	
Obj009C_CustomRunningAnim:
	dc.w $0003,$0008,$5D4A
	dc.w $0003,$0008,$5DB4
	dc.w $0003,$0008,$5E16

	dc.w $0002,$0008,$5CE0
	;dc.w $0002,$0008,$5D4A

	;dc.w $0003,$0008,$5D4A
	;dc.w $0003,$0008,$5DB4
	;dc.w $0003,$0008,$5E16

	;dc.w $0002,$0008,$5CE0
	;dc.w $0002,$0008,$5D4A

	;dc.w $0003,$0008,$5D4A
	;dc.w $0003,$0008,$5DB4
	;dc.w $0004,$0008,$5E16
	
	;dc.w $0002,$0008,$5D4A
	dc.w $0000,$0008,$5D4A

	;dc.w $0004,$0008,$5E68
	;dc.w $0002,$0008,$5EC2
	;dc.w $0000,$0008,$5EC2
	
; ---------------------------------------------------------------------------
; �������� ����������� � ���������
; ---------------------------------------------------------------------------
Obj009C_RNGtoBeh76:
	jsr     ($6AC).l ; random
	move.b	d0,$76(a0)
	andi.b	#$F3,(a0)
	jmp		$2BADE
	
;Obj009C_RNGtoBeh77:
;	jsr     ($6AC).l ; random
;	andi.b	#$1F,d0
;	move.b	d0,$77(a0)
;	jmp		$2AFBC
	
; ---------------------------------------------------------------------------
; ��������� �������� �������� ������������ ����
; ---------------------------------------------------------------------------
Obj009C_FlyingFix:
	tst.b	$6F(a0)
	beq		Obj009C_FlyingFix_10
	clr.b	$6F(a0)
	jmp		Obj009C_FlyingFix_ret

Obj009C_FlyingFix_10:
	move.b	#$10,$6F(a0)
	;jmp		Obj009C_FlyingFix_ret

Obj009C_FlyingFix_ret:
	move.b  #$14,$43(a0)
    addq.b  #1,$76(a0)
	jmp		$2BADE

 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
