;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; �������� ���������� ������� (1B5)
; ---------------------------------------------------------------------------
	org     $6354E
	jmp     loc_0_75F9E
	
	org     $6353A  ;��������� ����� �������� ����� �������� �������
	dc.w    $4C
	
	org     $63EDA
	;move.w  #$178,6(a1)
	jsr     (Obj1B5_63EDA).l
	
	org     $630D2
	nop
	nop
	nop
	
;� ������� ��
	org     $63068
	dc.w    $0900
	
; ---------------------------------------------------------------------------
; ������ ������� ��������� ��� ������
; ---------------------------------------------------------------------------
	org		$63090
	;bra.s	$630AA
	;lea		($A5E3A).l,a2
	;bra.s	$630A2
	jmp		Obj015B_NewDamagePalette

; ---------------------------------------------------------------------------
; ��������� ����� �������� ������� ������
; ---------------------------------------------------------------------------

	; landing wait
	org		$63D42
	move.w	#$0048,$74(a1) ; #$0078
	
	; standing (left arm) wait
	org		$63D74
	move.w	#$0036,$74(a1) ; #$0046

	; standing (right arm) wait
	org		$63DD2
	move.w	#$0022,$74(a1) ; #$0032

	; standing (still) wait
	org		$63E80
	move.w	#$0036,$74(a1) ; #$003C
	
; ---------------------------------------------------------------------------
; ��������� ����� �������� �������� ������� ����� ������
; ---------------------------------------------------------------------------
	org		$633B0
	move.w	#$001C,$74(a1) ; #$003C

; ---------------------------------------------------------------------------
; �������� �������� �������� �������
; ---------------------------------------------------------------------------
	; laser charge time
	org		$633B6
	move.w	#$002A,$76(a1) ; #$0064

	; stars
	org		$63436
	move.w	#$0007,$74(a1) ; #$000C
	
	; laser latence
	org		$6343E
	move.w	#$000E,$74(a1) ; #$001E

	; laser after wait
	;;;org		$63538
	;;;move.w	#$001C,$74(a1) ; #$004C
	;;; goto Obj015B_AltExplSpwan
	
; ---------------------------------------------------------------------------
; ��������� ����� �������� ������ ����� ������ ����������
; ---------------------------------------------------------------------------

	; robot
	org		$6368A
	move.w	#$0020,$74(a1) ; #$003C

	; robot hand
	org		$63F68
	move.w	#$0002,$74(a1) ; #$001E

; ---------------------------------------------------------------------------
; alter explosion spawn
; ---------------------------------------------------------------------------
	org		$634F0
	jmp		Obj015B_AltExplSpwan
; ---------------------------------------------------------------------------
; debug
; ---------------------------------------------------------------------------
	;; despawn explosion
	;org		$63AD8
	;nop
	;nop
	
	;; despawn explosion sound
	;org		$63510
	;nop
	;nop

	;; despawn explosion sound
	;org		$63522
	;nop
	;nop

 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; ���������� ������ �������
; ---------------------------------------------------------------------------

loc_0_75F9E:	            ; CODE XREF: ROM:00075F92j
	tst.w   $4(a1)
	beq.w   loc_0_75F48

	tst.w   $74(a1)
	beq.w   loc_0_635AC
	tst.l   $E(a1)
	bmi.s   loc_1_75F9E
	cmpi.w  #$E0,6(a1) ; '�'
	bgt.s   loc_0_75FA6
	bra.s   loc_0_75FAA
	
loc_1_75F9E
	cmpi.w  #-$20,6(a1)
	bgt.s   loc_0_75FAA

loc_0_75FA6:	            ; CODE XREF: ROM:00075F9Aj
	neg.l   $E(a1)

loc_0_75FAA:	            ; CODE XREF: ROM:00075F9Cj
		        ; ROM:00075FA4j
	clr.l   d0
	move.w  6(a1),d0
	move.l  #$100C0,d1
	sub.l   d0,d1
	move.w  d1,($FFFFA0F8).w
	addi.l  #$2000,$12(a1)
	bmi.s   locret_0_75FEA
	cmpi.w  #$128,$A(a1) ; '(' ; bottom
	blt.s   locret_0_75FEA
	move.l  #$A0000,d0
	move.l  #$C000,d1
	move.w  #1,d2
	jsr     $807E
	move.w  #$81,d0 ; '�'
	jsr     $A8042
	move.w  #$128,$A(a1) ; '('
	move.l  #$FFFBC000,$12(a1)

locret_0_75FEA:	         ; CODE XREF: ROM:00075FB2j
		        ; ROM:00075FBAj
	btst    #0,$24(a1)
	bne.w   Obj1B5_CheckY
	rts
	
Obj01B5_TimerTable:
	dc.w	$44,$88,$CC,$88
	
;��������� � �������
loc_0_75F48:	            ; DATA XREF: ROM:off_0_75F22o
	addi.l  #$2000,$12(a1)
	cmpi.w  #$28,$A(a1) ; '('
	blt.s   locret_0_75F8C
	;move.l  #$A0000,d0
	;move.l  #$C000,d1
	;move.w  #1,d2
	;jsr     $807E
	move.w  #$81,d0 ; '�'
	jsr     $A8042
	;move.l  #-$50000,$12(a1)
	bclr    #0,$24(a1)
	
	jsr     ($6AC).l ; random
	move.w	d0,d1
	swap	d0
	and.w	#$6,d0
	move.w	Obj01B5_TimerTable(pc,d0.w),d0
	move.w  d0,$74(a1)
	
	;move.w  #($40*3),$74(a1)    ;$183 ��� $102
	
	tst.l   $20(a1)
	beq.s   Obj1B5_Null
	move.w  $20(a1),$E(a1)
	bra.s   Obj1B5_NoNull
Obj1B5_Null:
	and.w	#$4,d1
	move.l  Obj01B5_SpeedTable(pc,d1.w),d0
	move.l  d0,$E(a1)
Obj1B5_NoNull:
	addq.w  #1,$4(a1)

locret_0_75F8C:	         ; CODE XREF: ROM:00075F56j
	rts
	
Obj01B5_SpeedTable:
	dc.l	$20000,-$20000
	
loc_0_635AC:    
	move.w  $E(a1),$20(a1)
	clr.l   $E(a1)
	;clr.l   $12(a1)
	bset    #0,$24(a1)
	rts
	
Obj1B5_CheckY:
	cmpi.w  #$00E0,$A(a1)
	bgt.s   locret_0_75F8C
;	bra.s   Obj1B5_Cmon
;Obj1B5_Cmon:
	clr.l   $12(a1)
	jmp $635AC
	
Obj1B5_63EDA:
	move.w  ($FFFFB006).w,d0
	add.w   #$138,d0
	move.w  d0,6(a1)
	;move.w  #$198,6(a1)
	rts

; ---------------------------------------------------------------------------
; ����� �������� ������� ��� ������
; ---------------------------------------------------------------------------
Obj015B_NewDamagePalette:
	jsr     ($6AC).l
	lea		($A5E1A).l,a2;lea		($A5E1A).l,a2
	andi.l	#$20,d0
	add.w	d0,d0
	add.w	d0,d0
	;asr.w	#1,d0
	adda.l	d0,a2
	jmp		$630A2

; ---------------------------------------------------------------------------
; alter explosion spawn
; ---------------------------------------------------------------------------
Obj015B_AltExplSpwan:
	addq.b  #1,($FFFF9802).w
	move.w  #$C,d5
	lea     ($FFFF9844).w,a5
	jsr     $59820 ; Fill Free Object Slot List
	jsr     ($6AC).l ; random
	and.w	#$3F,d0
	add.w	#$80,d0
	;move.w  #$80,d0 ; '�'
	;move.w  #1,d1
	move.w	#0,d3
	move.w	Obj015B_AltExplSpwan_times(pc,d3.w),d1

loc_6350A:                              ; CODE XREF: sub_6338E+1A8j

	; explosion
	move.l  (a5)+,d2
	beq.s   loc_63538
	movea.l d2,a6
	move.w  #$1B8,(a6) ; spawn explosion
	move.w  d0,6(a6)
	move.w  d1,$74(a6)
	
	; fire
	move.l  (a5)+,d2
	beq.s   loc_63538
	movea.l d2,a6
	move.w  #$35B,(a6) ; spawn explosion fire
	move.w  d0,6(a6)
	move.w  d1,$74(a6)
	
	; move + time
	addi.w  #$40,d0 ; '@'
	;addi.w  #$11,d1
	addq.w  #2,d3
	move.w	Obj015B_AltExplSpwan_times(pc,d3.w),d1
	
	bra.s   loc_6350A
; ---------------------------------------------------------------------------

loc_63538:                              ; CODE XREF: sub_6338E+17Ej
							; sub_6338E+190j
	move.w  #$1C,$74(a1) ; '�' wait
	addq.w  #1,4(a1)
	rts
	
Obj015B_AEST_BASE	equ $2A
Obj015B_AEST_INCR	equ $E
	
Obj015B_AltExplSpwan_times:
	;dc.w	$18, $22, $2E, $2E, $22, $18
	dc.w	Obj015B_AEST_BASE + Obj015B_AEST_INCR * 2
	dc.w	Obj015B_AEST_BASE + Obj015B_AEST_INCR * 1
	dc.w	Obj015B_AEST_BASE + Obj015B_AEST_INCR * 0
	dc.w	Obj015B_AEST_BASE + Obj015B_AEST_INCR * 0
	dc.w	Obj015B_AEST_BASE + Obj015B_AEST_INCR * 1
	dc.w	Obj015B_AEST_BASE + Obj015B_AEST_INCR * 2

 endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
