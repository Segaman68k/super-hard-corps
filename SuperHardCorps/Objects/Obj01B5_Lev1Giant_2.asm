;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; �������� ���������� ������� (1B5)
; ---------------------------------------------------------------------------
	org     $6354E
	jmp     loc_0_75F9E
	org     $6353A  ;��������� ����� �������� ����� �������� �������
	dc.w    $4C
	org     $63EDA
	;move.w  #$178,6(a1)
	jsr     (Obj1B5_63EDA).l
	org     $630D2
	nop
	nop
	nop
;� ������� ��
	org     $63068
	dc.w    $0C00

 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; ���������� ������ �������
; ---------------------------------------------------------------------------

loc_0_75F9E:	            ; CODE XREF: ROM:00075F92j
	tst.w   $4(a1)
	beq.w   loc_0_75F48

	tst.w   $74(a1)
	beq.w   loc_0_635AC
	tst.l   $E(a1)
	bmi.s   loc_1_75F9E
	cmpi.w  #$E0,6(a1) ; '�'
	bgt.s   loc_0_75FA6
	bra.s   loc_0_75FAA
	
loc_1_75F9E
	cmpi.w  #-$20,6(a1)
	bgt.s   loc_0_75FAA

loc_0_75FA6:	            ; CODE XREF: ROM:00075F9Aj
	neg.l   $E(a1)

loc_0_75FAA:	            ; CODE XREF: ROM:00075F9Cj
		        ; ROM:00075FA4j
	clr.l   d0
	move.w  6(a1),d0
	move.l  #$100C0,d1
	sub.l   d0,d1
	move.w  d1,($FFFFA0F8).w
	addi.l  #$2000,$12(a1)
	bmi.s   locret_0_75FEA
	cmpi.w  #$140,$A(a1) ; '('
	blt.s   locret_0_75FEA
	move.l  #$A0000,d0
	move.l  #$C000,d1
	move.w  #1,d2
	jsr     $807E
	move.w  #$81,d0 ; '�'
	jsr     $A8042
	move.w  #$140,$A(a1) ; '('
	move.l  #$FFFB0000,$12(a1)

locret_0_75FEA:	         ; CODE XREF: ROM:00075FB2j
		        ; ROM:00075FBAj
	btst    #0,$24(a1)
	bne.w   Obj1B5_CheckY
	rts
	
;��������� � �������
loc_0_75F48:	            ; DATA XREF: ROM:off_0_75F22o
	addi.l  #$2000,$12(a1)
	cmpi.w  #$28,$A(a1) ; '('
	blt.s   locret_0_75F8C
	;move.l  #$A0000,d0
	;move.l  #$C000,d1
	;move.w  #1,d2
	;jsr     $807E
	move.w  #$81,d0 ; '�'
	jsr     $A8042
	;move.l  #-$50000,$12(a1)
	bclr    #0,$24(a1)
	move.w  #$90,$74(a1)    ;$183 ��� $102
	tst.l   $20(a1)
	beq.s   Obj1B5_Null
	move.w  $20(a1),$E(a1)
	bra.s   Obj1B5_NoNull
Obj1B5_Null:
	move.l  #$20000,$E(a1)
Obj1B5_NoNull:
	addq.w  #1,$4(a1)

locret_0_75F8C:	         ; CODE XREF: ROM:00075F56j
	rts
	
loc_0_635AC:    
	move.w  $E(a1),$20(a1)
	clr.l   $E(a1)
	;clr.l   $12(a1)
	bset    #0,$24(a1)
	rts
	
Obj1B5_CheckY:
	cmpi.w  #$00E0,$A(a1)
	bgt.s   locret_0_75F8C
;	bra.s   Obj1B5_Cmon
;Obj1B5_Cmon:
	clr.l   $12(a1)
	jmp $635AC
	
Obj1B5_63EDA:
	move.w  ($FFFFB006).w,d0
	add.w   #$138,d0
	move.w  d0,6(a1)
	;move.w  #$198,6(a1)
	rts

 endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
