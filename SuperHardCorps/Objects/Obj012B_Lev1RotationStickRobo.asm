;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0
 
Obj012B_RotSpeed1		equ $100;$180
Obj012B_RotSpeed2		equ $140;$180
Obj012B_RotSpeed3		equ $180;$180
Obj012B_RotSpeed4		equ $1C0;$180

; ---------------------------------------------------------------------------
; ��������� �������� ������ 121
; ---------------------------------------------------------------------------
	org		$4BF78
	;move.w	#Obj012B_RotSpeed,$4A(a1)
	;addq.w	#1,$2(a1)
	;moveq	#$F,d7
	;jmp		($59658).l
	jmp		Obj012B_RotationRandomizer

; ---------------------------------------------------------------------------
; �������� ������
; ---------------------------------------------------------------------------
	org		$4C102
	;cmpi.w	#$F800,$4A(a1);#$E400,$4A(a1)
	move.w	#$F800,$4A(a1)
	bra		$4C10A

	org		$4C1B2
	move.w	#$24,$4E(a1);#$C8,$4E(a1)

 endif
 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

Obj012B_RotationRandomizer:

	move.l	a0,-(sp)
	lea		Obj012B_RotationSpeedList,a0
	jsr     ($6AC).l; cast random number
	addi.w	#$C,d0
	andi.l	#$E,d0
	;adda.l	d0,a0
	;move.w	#Obj012B_RotSpeed,$4A(a1)
	move.w	(a0,d0.w),$4A(a1)
	move.l	(sp)+,a0
	addq.w	#1,$2(a1)
	moveq	#$F,d7
	jmp		($59658).l

Obj012B_RotationSpeedList:
	dc.w	-Obj012B_RotSpeed1,Obj012B_RotSpeed1
	dc.w	Obj012B_RotSpeed2,-Obj012B_RotSpeed2
	dc.w	-Obj012B_RotSpeed3,Obj012B_RotSpeed3
	dc.w	Obj012B_RotSpeed4,-Obj012B_RotSpeed4
	
 endif
