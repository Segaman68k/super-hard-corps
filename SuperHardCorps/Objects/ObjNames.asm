;============================================================
; ������ ��������� ��������
;============================================================

ObjID_Player					equ		$0001

; DEBUG

ObjID_CollisionDebug			equ		$0008
ObjID_EventOrbDebug				equ		$0009
ObjID_EventSkip					equ		$0014

; PARTICLE

ObjID_ParticleExplosion			equ		$0010

; SPECIAL

ObjID_WeaponBirdA				equ		$001D
ObjID_WeaponBirdB				equ		$001E
ObjID_WeaponBirdC				equ		$001F
ObjID_WeaponBirdD				equ		$0020
ObjID_WeaponBirdE				equ		$0021

; LEVEL 1

ObjID_Lev1EventMaster			equ		$0014
ObjID_Lev1BuildingWindows		equ		$001A
ObjID_Lev1BuildingWindowsSpawner		equ		$001B
ObjID_Lev1ArmoredCarrier		equ		$002C
ObjID_Lev1GasolineTanker		equ		$002D
ObjID_Lev1Spider				equ		$009A
ObjID_Lev1Boss					equ		$009C
ObjID_Lev1AgroRobotSlash		equ		$0120
ObjID_Lev1AgroRobotShot			equ		$0121
ObjID_Lev1AgroRobotSniperExp1	equ		$0124
ObjID_Lev1AgroRobotBomb			equ		$0125
ObjID_Lev1AgroRobotSpawner		equ		$012A
ObjID_Lev1RotationStickRobo		equ		$012B
ObjID_Lev1RotationStickRoboPart	equ		$012C
ObjID_Lev1RotationStickAgroRobo	equ		$012E
ObjID_Lev1Earthquake			equ		$0138
ObjID_Lev1AgroRobotSniperExp2	equ		$014D
ObjID_Lev1AgroRobotSniper		equ		$0167
ObjID_Lev1Cendepide				equ		$0181
ObjID_Lev1CendepideSpawner		equ		$0183
ObjID_Lev1CendepideIntro		equ		$01A7
ObjID_Lev1GiantTiny				equ		$01B0
ObjID_Lev1Giant					equ		$01B5
ObjID_Lev1AgroRobotBombIntro	equ		$02B9
ObjID_Lev1AgroRobotSlashIntro	equ		$02BA
ObjID_Lev1AgroRobotShotIntro	equ		$02BB

; LEVEL 3

ObjID_Lev3DoorEvent				equ		$000F
ObjID_Lev3Door					equ		$002A

; LEVEL 5

ObjID_Lev5GiantDoor				equ		$0015
