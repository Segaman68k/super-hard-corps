;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0
 
; ---------------------------------------------------------------------------
; �������� ����� ������������
; ---------------------------------------------------------------------------
	org		$4BBEE
	jmp		Obj012A_check4death
	
; ---------------------------------------------------------------------------
; ������ �������� ����, ���� �� �������� ������� �����, � ��������� �� �������� ������
; ---------------------------------------------------------------------------
	org		$4BBC4
	jmp		Obj012A_check4corner
	;bra		$4BBD0
	
; ---------------------------------------------------------------------------
; �������� ��������� ������-������
; ---------------------------------------------------------------------------
	org		$4BBD0
	jmp		Obj012A_actLikeEvent

; ---------------------------------------------------------------------------
; �������� ���������� ������ �� ������
; ---------------------------------------------------------------------------
	org		$4BBB6
	jmp		Obj012A_BehOnSpawn

 endif
 
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; �������� ����� ������������
; ---------------------------------------------------------------------------
Obj012A_check4death:
	movem.w	d0-d1,-(sp)
	move.w	(ScreenXPOS).w,d0
	move.w	6(a1),d1
	cmp.w	d1,d0
	bls		Obj012A_nomames
	movem.w	(sp)+,d0-d1
	tst.w	$A(a1)
	bne		Obj012A_despawn
	clr.w	(a1)
	rts
Obj012A_nomames:
	movem.w	(sp)+,d0-d1
	jmp		$4BBFA
	
	
	
; ---------------------------------------------------------------------------
; ������ �������� ����, ���� �� �������� ������� �����, � ��������� �� �������� ������
; ---------------------------------------------------------------------------
Obj012A_check4corner:
	movem.w	d0-d1,-(sp)
	move.w	(ScreenXPOS).w,d0
	move.w	6(a1),d1
	sub.w	#$50,d1
	cmp.w	d1,d0
	bls		Obj012A_nomames2
	movem.w	(sp)+,d0-d1
	addq.w	#1,2(a1)
	jmp		$4BBD0
Obj012A_nomames2:
	movem.w	(sp)+,d0-d1
	jmp		$4BBD0
	
; ---------------------------------------------------------------------------
; �������� ��������� ������-������
; ---------------------------------------------------------------------------
Obj012A_actLikeEvent:
	tst.w	$A(a1)
	beq		Obj012A_actLikeEvent_dont
	
	; lock camera
	tst.w	$50(a1)
	bne		Obj012A_skipCameraLock
	move.l	#$00004000,d0
	cmp.w	#$1000,$A(a1)
	blt		Obj012A_skipCameraLock_slow
	add.l	#$00007000,d0
Obj012A_skipCameraLock_slow:
	move.w	#$0100,(ScreenScrollMode).w
	move.l	d0,($FFFFA0F0).w
	move.w	#1,$50(a1)
Obj012A_skipCameraLock:
	
	; save checkpoint
	tst.w	$48(a1)
	bne		Obj012A_actLikeEvent_2
	jsr		Obj012A_actLikeEvent_saveCheckpoint
	
Obj012A_actLikeEvent_2:

	; count
	subq.w	#2,$A(a1)
	beq		Obj012A_despawn
	
	; check if can spawn
	move.w	$A(a1),d0
	move.w	$A(a1),d1
	move.w	$A(a1),d2
	
	and.w	#$80,d0 ; period
	beq		Obj012A_actLikeEvent_waitSetRandType
	and.w	#$40,d1 ; sub period
	beq		Obj012A_actLikeEvent_waitSetRandType
	;and.w	#$8,d2  ; spawn
	;beq		Obj012A_actLikeEvent_wait
	
	; checkpoint
	move.w	$48(a1),d0
	move.w	$A(a1),d1
	cmp.w	d0,d1
	bne		Obj012A_actLikeEvent_wait
	jsr		Obj012A_actLikeEvent_saveCheckpoint
	move.w	$4A(a1),d0 ; take random number, generated before
	jmp		$4BC0A
	
Obj012A_actLikeEvent_waitSetRandType:
	; make random number
	move.w	$48(a1),d0
	cmp.w	$4C(a1),d1
	beq		Obj012A_actLikeEvent_waitReset
	
	move.w	d0,$4C(a1)
	jsr     ($6AC).l ; random
	swap    d0
	andi.w  #$C,d0
	move.w	d0,$4A(a1) ; save to robo spawner type
	
Obj012A_actLikeEvent_waitReset:

	; reset checkpoint
	clr.w	$48(a1)
	
Obj012A_actLikeEvent_wait:
	rts
	
Obj012A_actLikeEvent_saveCheckpoint:
	move.w	$A(a1),d0
	subq.w	#1,d0
	and.w	#$FFF0,d0
	move.w	d0,$48(a1)
	
	rts

Obj012A_despawn:
	move.w	$44(a1),(ScreenScrollMode).w
	clr.l   (a1)
	rts
	
	;------------------------------------
Obj012A_actLikeEvent_dont:
	subq.w  #1,$40(a1)
	bmi.s   Obj012A_4BBD8
	rts
	
Obj012A_4BBD8:
	jmp		$4BBD8
	
; ---------------------------------------------------------------------------
; �������� ���������� ������ �� ������
; ---------------------------------------------------------------------------
Obj012A_BehOnSpawn:
	move.b  #2,$1C(a1)
	clr.w   $40(a1)
	addq.w  #1,2(a1)
	; save screen scroll
	move.w	(ScreenScrollMode).w,$44(a1)
	jmp		$4BBC4 ; return

 endif
