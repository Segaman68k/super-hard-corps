;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0
 
 ; ---------------------------------------------------------------------------
; Insert object to array
; ---------------------------------------------------------------------------
	org		$292E
	dc.l	Obj027F_DebuggerObject

 endif
 
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; ---------------------------------------------------------------------------
; vars
Obj027F_max	equ	$47F

; regs
Obj027F_id		equ	$40
Obj027F_subid	equ	$42
Obj027F_draw	equ	$44
Obj027F_item	equ	$45
; ---------------------------------------------------------------------------
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Obj027F_DebuggerObject:

	move.w  ObjSubID(a1),d0
	add.w   d0,d0
	move.w  offObj027F_Behavior(pc,d0.w),d0
	jsr     offObj027F_Behavior(pc,d0.w)
	tst.b	Obj027F_draw(a1)
	beq		+
	jsr     ($574C).l
+:
	rts

; ---------------------------------------------------------------------------
offObj027F_Behavior:
	dc.w Obj027F_BehInit-offObj027F_Behavior
	dc.w Obj027F_BehAct-offObj027F_Behavior
	dc.w Obj027F_BehLocked-offObj027F_Behavior
; ---------------------------------------------------------------------------

Obj027F_BehInit:

	; change mode
	addq.w	#1,ObjSubID(a1)
	move.w	#0,Obj027F_id(a1)
	move.w	#0,Obj027F_subid(a1)
	move.w	#0,Obj027F_item(a1)
	
	move.b	#1,d0
	jsr		Obj027F_InitAnim
	
	;move.l	a1,ObjANIM(a1)
	;add.w	#$20,2+ObjANIM(a1)
	
	;clr.l	(a1)
	
	rts
	
Obj027F_InitAnim:
	tst.b	d0
	beq		Obj027F_InitAnim_null
	move.l  #$9AB0,$26(a1) ; star
	move.b	#1,Obj027F_draw(a1)
	rts
	
Obj027F_InitAnim_null:
	move.l	a1,ObjANIM(a1)
	add.l	#$20,ObjANIM(a1)
	move.l	a1,ObjSPR(a1)
	add.l	#$20,ObjSPR(a1)
	move.b	#0,Obj027F_draw(a1)
	rts

; ---------------------------------------------------------------------------

Obj027F_BehAct:
	
	move.w	(Joy2pr).w,d0
	move.w	(Joy2).w,d2

	; check if mode is right
	cmp.w	#1,ObjSubID(a1)
	bne		Obj027F_BehAct_noAct
	
	move.w	d2,d1
	and.w	#$2000,d1 ; btn C
	beq		+
	
	; spawn object
	jsr		Obj027F_BehAct_Spawn
	bra		Obj027F_BehAct_noAct
	
+:
	move.w	d2,d1
	and.w	#$4000,d1 ; btn A
	beq		+
	
	; setup ID
	move.l	a1,a2
	add.l	#Obj027F_id,a2		; address
	move.w	#Obj027F_max,d2		; max value
	
	jsr		Obj027F_BehAct_Number
	bra		Obj027F_BehAct_noAct
+:
	move.w	d2,d1
	and.w	#$1000,d1 ; btn B
	beq		+
	
	; setup SubID
	move.l	a1,a2
	add.l	#Obj027F_subid,a2	; address
	move.w	#$7FFE,d2			; max value
	
	jsr		Obj027F_BehAct_Number
	bra		Obj027F_BehAct_noAct
+:

	; move coords
	jsr		Obj027F_BehAct_Coords
	
Obj027F_BehAct_noAct:
	jsr		Obj027F_BehAct_Letter
	rts

; ---------------------------------------------------------------------------
;;; change number
; a2 - value address
; d2 - max value
; ---------------------------------------------------------------------------


Obj027F_BehAct_Number:
	move.w	(a2),d1
	
	; up
	cmp.w	#$0100,d0
	bne		+
	subq.w	#1,d1
	bpl		+
	move.w	d2,d1
+:
	; down
	cmp.w	#$0200,d0
	bne		+
	addq.w	#1,d1
	cmp.w	d2,d1
	ble		+
	move.w	#$0,d1
+:
	; left
	cmp.w	#$0400,d0
	bne		+
	sub.w	#$20,d1
	bpl		+
	move.w	d2,d1
+:
	; right
	cmp.w	#$0800,d0
	bne		+
	add.w	#$20,d1
	cmp.w	d2,d1
	ble		+
	move.w	#$0,d1
+:
	move.w	d1,(a2)
+:
	rts
 
; ---------------------------------------------------------------------------
;;; coords
; ---------------------------------------------------------------------------

Obj027F_BehAct_Coords:
	move.w	d2,d1
	and.w	#$0100,d1 ; btn U
	beq		+
	sub.w	#1,ObjY(a1)
+:

	move.w	d2,d1
	and.w	#$0200,d1 ; btn D
	beq		+
	add.w	#1,ObjY(a1)
+:

	move.w	d2,d1
	and.w	#$0400,d1 ; btn L
	beq		+
	sub.w	#1,ObjX(a1)
+:

	move.w	d2,d1
	and.w	#$0800,d1 ; btn R
	beq		+
	add.w	#1,ObjX(a1)
+:

	rts
; ---------------------------------------------------------------------------
;;; spawn
; ---------------------------------------------------------------------------
Obj027F_BehAct_Spawn:

	; get free slot
	jsr		$59730 ; subGetFreeObjectSlot_59730
	bne		Obj027F_spawnNo
	
	; obj ID
	;move.w	Obj027F_id(a1),		ObjID(a6)
	;move.w	Obj027F_subid(a1),	ObjSubID(a6)
	move.l	Obj027F_id(a1),		ObjID(a6)
	
	; obj coords
	move.l	ObjX(a1),ObjX(a6)
	move.l	ObjY(a1),ObjY(a6)
	
	; change mode to locked
	addq.w	#1,ObjSubID(a1)
	
	; remove animation
	move.b	#0,d0
	jsr		Obj027F_InitAnim
	
	rts

Obj027F_spawnNo:
	move.w  #$81,d0 ; 'Б' ; giant stomp
	jsr     $A8042 ; play sound
	rts

; ---------------------------------------------------------------------------
;;; draw letters
; ---------------------------------------------------------------------------
Obj027F_BehAct_Letter:

	move.w	Obj027F_id(a1),d0
	moveq	#8 + 5*0,d6
	moveq	#1,d7
	jsr     $C93C; sub_C93C ; print word
	
	move.w	Obj027F_subid(a1),d0
	moveq	#8 + 5*1,d6
	moveq	#1,d7
	jsr     $C93C; sub_C93C ; print word
	
	move.w	ObjX(a1),d0
	moveq	#8 + 5*2,d6
	moveq	#1,d7
	jsr     $C93C; sub_C93C ; print word
	
	move.w	ObjY(a1),d0
	moveq	#8 + 5*3,d6
	moveq	#1,d7
	jsr     $C93C; sub_C93C ; print word
	
	rts
		
; ---------------------------------------------------------------------------
Obj027F_BehLocked:
	cmp.w	#$4000,(Joy2pr).w
	bne		+
	;clr.w	ObjSubID(a1)
	subq.w	#1,ObjSubID(a1)
	move.b	#1,d0
	jsr		Obj027F_InitAnim
+:
	rts
; ---------------------------------------------------------------------------
 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
