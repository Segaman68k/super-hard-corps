; ---------------------------------------------------------------------------
; Contra Hard Corps - Super Hard Corps
;
; Author: Mikhail 'Segaman' Shilenko
; Created 31.01.2013
; ---------------------------------------------------------------------------

; ---------------------------------------------------------------------------
; Configure compilator AS
; ---------------------------------------------------------------------------

	CPU 68000
	padding off ; we don"t want AS padding out dc.b instructions
	listing off ; we don"t need to generate anything for a listing file
	supmode on  ; we don"t need warnings about privileged instructions

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; 			Useful macros and constants
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	include		SuperHardCorps\ROM.asm
	include		SuperHardCorps\RAM.asm
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; 			Build configuraton
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

modlistcount set 0

turnonlevsel		= 0
turnontestlev		= 1
intro_text			= 1
pal_fix				= 1
damage_fix			= 0
hit_points			= 1
enable_cheat_codes	= 1

z80mod				= 0

LevelList			= 1
Level0ObjectList	= 1
Level1ObjectList	= 1
Level4ObjectList	= 1
Level4Boss			= 1

Obj0001_Player						= 1
Obj002C_Lev1ArmoredCarrier			= 1
Obj0120_121_122_123_Lev1MadRobots	= 1
Obj012A_Lev1AgroRobotSpawner		= 1
Obj012B_Lev1RotationStickRobo		= 1
Obj0181_Lev1Cendepide				= 1
Obj01B5_Lev1Giant					= 1
Obj009A_Lev1Spider					= 1
Obj009C_Lev1Boss					= 1

Obj027F_Debugger	= 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ---------------------------------------------------------------------------
; Insert ROM Image
; ---------------------------------------------------------------------------
	org	$0
	binclude	"Contra - Hard Corps (U).bin"

; ---------------------------------------------------------------------------
; Header
; ---------------------------------------------------------------------------
	org $120
	dc.b "CONTRA SUPER HARD CORPS - ELEKTROPAGE.RU        "
	dc.b "CONTRA SUPER HARD CORPS - ELEKTROPAGE.RU        "
	org $1F0
	dc.b "EUJ"
	
; ---------------------------------------------------------------------------
; Skip checksum
; ---------------------------------------------------------------------------
	org $384
	jmp $39E
	
; ---------------------------------------------------------------------------
; Skip Region lock
; ---------------------------------------------------------------------------
	org $3AE
	bra.s $3C6

; ---------------------------------------------------------------------------
; Setup level select
; ---------------------------------------------------------------------------
 if turnonlevsel = 1
	org		$90DC
	bra.s	$90E8
 endif
 
; ---------------------------------------------------------------------------
; Setup level select
; ---------------------------------------------------------------------------
 if turnontestlev = 1
	org		$7BACC
	dc.w	$D ; $C
 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

endofrom set 1

	org     $1FC000
	
 ;if * < $1FC000
 ;error "too low address : $\{(*)}"
 ;endif
	
; ---------------------------------------------------------------------------
; Include mods in End of ROM Space
; ---------------------------------------------------------------------------
	include SuperHardCorps\Macrosetup.asm
	include SuperHardCorps\IntroText\IntroText_macro.asm
	include SuperHardCorps\Level\LevelObject_macro.asm
	include SuperHardCorps\Objects\ObjNames.asm
	
	include SuperHardCorps\ModList.asm

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

endofrom set 0  ; we're not at the end of rom

; ---------------------------------------------------------------------------
; Include mods in ROM Space
; ---------------------------------------------------------------------------
	include SuperHardCorps\ModList.asm

; ---------------------------------------------------------------------------
; Errors
; ---------------------------------------------------------------------------
 if modlistcount <> 2
 error "ModList inserted less than 2 times : $\{(modlistcount)}"
 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
