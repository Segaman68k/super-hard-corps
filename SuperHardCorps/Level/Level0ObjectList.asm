;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0
		org			$7BE06
		dc.l		Level00_ObjectList
 
 endif
 
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

		;org		$7D7D6
Level00_ObjectList:
        ;LevObj             $0381,$0000,$0000,$0000,$0120,$00F0,$B001   ; ROM : 0x0007D7D6
        LevObj             $027F,$0000,$0000,$0000,$0120,$00F0,$B001
        LevScrollSetEx     $0000,$0000,$0000,$0000,$4000,$4000,player,Free   ; ROM : 0x0007E476
        LevWaitEvent
        LevObj             $01D1,$2020,$0100,$0000,$0000,$0000,$B001   ; ROM : 0x0007E6BE
        LevWaitEvent                                                   ; ROM : 0x0007E6CC
        LevExecPalEffect   $0000                                       ; ROM : 0x0007E6CE
        LevWaitPalEffectEnd                                            ; ROM : 0x0007E6D2
        LevEnd                                                         ; ROM : 0x0007E6D4

 
 endif
