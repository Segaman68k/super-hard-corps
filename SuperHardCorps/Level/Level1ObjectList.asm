;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
; $7C0BA - $7C112 - Config 1 LEVEL
; $CEDA - Level Load
; macro ProcLEvelCode unk,loc
;	dc.w	-2
;	dc.l	unk
;	dc.l	loc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; ��������� �� ������ �������� ������ 1
; ---------------------------------------------------------------------------
		org		$7C0F8
		dc.l	Level0C_ObjectList;$7E1D8
		
; ---------------------------------------------------------------------------
; ��������� �� ������ �������� ������ 1
; ---------------------------------------------------------------------------
		org		$7C0FC
		dc.l	Level0C_CollList;$7C26E
		
; ---------------------------------------------------------------------------
; ��������� �� ����� � ����� ������
; ---------------------------------------------------------------------------
		org		$7C0F0
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
		
		org		$7C148
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
		
		org		$7C1A0
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
		
		org		$7C1F4
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
		
		org		$7C24C
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
		
		org		$7CFA0
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7CFF8
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D050
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D0A8
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D18C
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D1DC
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D288
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D46E
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D56A
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D616
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D67E
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
		org		$7D6CE
		dc.l	$1E391A
		dc.l	Level1Map;$1E597C
	
 
		;org		$7E1E8
		;dc.w	$9C;$2C
		
		;org		$7D7D6
		;dc.w	$9C;$2C
		
		;org		$7E1F6
		;dc.w	$2;$1A7
 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 2

; ---------------------------------------------------------------------------
; ������ �������� ������ 1 ($0C)
; ---------------------------------------------------------------------------
Level0C_ObjectList:
		dc.w	$FFFD,$0000,$0100,$0101,$0000,$0000,$0A00,$0000
		dc.w	$002C,$0000,$0100,$0000,$0090,$0090,$B001
		dc.w	$0167,$0000,$0100,$0000,$00B0,$00E8,$B001
		dc.w	$02BA,$0000,$0100,$0000,$0100,$0148,$B001
		dc.w	$02B9,$0000,$0100,$0000,$0120,$0148,$B001
		dc.w	$02BA,$0000,$0100,$0000,$0180,$0148,$B001
		;dc.w	$02BB,$0000,$0100,$0000,$01A8,$0138,$B001
		dc.w	$0167,$01F0,$0100,$0000,$01C8,$00E0,$B001
		dc.w	$0167,$0260,$0100,$0000,$01C8,$00E0,$B001
		dc.w	$02B9,$0280,$0100,$0000,$01C8,$0118,$B001
		;dc.w	$01A7,$0400,$0100,$0000,$0000,$0000,$B081
		dc.w	$0183,$0400,$0100,$0000,$0000,$0000,$B001
		dc.w	$012A,$0440,$0100,$0000,$0000,$0000,$B001
		dc.w	$0167,$05E0,$0100,$0001,$05E0,$0161,$B001
		dc.w	$0167,$0650,$0100,$0001,$0650,$0161,$B001
		dc.w	$0124,$0814,$0100,$0001,$0814,$0169,$B001
		dc.w	$014D,$0934,$0100,$0001,$0934,$0169,$B001
		dc.w	$001D,$0938,$0100,$0000,$0060,$0100,$B001
		dc.w	$0020,$0950,$0100,$0000,$01C8,$0100,$B001
		dc.w	$0125,$09C0,$0100,$0001,$09C0,$01AE,$B001
		dc.w	$002D,$0A00,$0100,$0001,$0A00,$0100,$B201
		dc.w	$0167,$0AF8,$0100,$0001,$0AF8,$0182,$B001
		dc.w	$FFFF
		dc.w	$FFFD,$0000,$0100,$0101,$0000,$0000,$0E00,$0100
		dc.w	$0183,$0C30,$0100,$0000,$0000,$0000,$B001
		dc.w	$0138,$0C50,$0100,$0000,$0000,$0000,$B001
		dc.w	$FFFE,$0C00,$0100
		dc.l	$0000C996
		dc.w	$0021,$0E80,$0100,$0000,$01D0,$0100,$B301
		dc.w	$FFFE,$0DE0,$0100
		dc.l	$000067DE
		dc.w	$FFF9,$0E00,$0100,$0013,$9904,$FFFF,$0000,$3000,$0800
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF9,$0E00,$0100,$0013,$A252,$FFFF,$0000,$4000,$1000
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF9,$0E00,$0100,$0013,$B0EE,$FFFF,$0000,$7800,$0400
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF7,$0E00,$0100,$FFFF,$A18C,$0001,$7698
		dc.w	$FFF7,$0E00,$0100,$FFFF,$A190,$0000,$0000
		dc.w	$FFFC,$0E00,$0100,$001E,$4AB6,$FFFF,$0000
		dc.w	$FFF6
		dc.w	$FFFB,$0E00,$0100,$FFFF,$0000,$E000,$0800
		dc.w	$FFF5
		dc.w	$009A,$0F60,$0100,$0000,$0120,$0130,$B000
		dc.w	$FFFF
		dc.w	$001B,$0E00,$0100,$0000,$0090,$0090,$C380
		dc.w	$FFFD,$0E00,$0100,$0101,$0E00,$0000,$0EC0,$0100
		dc.w	$FFFE,$0E00,$0100
		dc.l	$000177CE
		dc.w	$FFFE,$0EC0,$0100
		dc.l	$00017800
		dc.w	$FFF7,$0EC0,$0100,$FFFF,$A18C,$0001,$76E8
		dc.w	$FFF7,$0EC0,$0100,$FFFF,$A190,$0000,$7858
		dc.w	$FFFE,$0EC0,$0100
		dc.l	$00017800
		dc.w	$FFFA,$0EC0,$0100,$0001,$7820
		dc.w	$0014,$0EC0,$0100,$0000,$0090,$0090,$B001
		dc.w	$FFFF
		dc.w	$FFEE,$0EC0,$0100,$FFFF,$A34E,$0100
		dc.w	$FFFE,$0E00,$0100
		dc.l	$00017592
		dc.w	$FFF7,$0E00,$0100,$FFFF,$A18C,$0000,$0000
		dc.w	$FFF7,$0E00,$0100,$FFFF,$A190,$0000,$0000
		dc.w	$FFFE,$0E00,$0100
		dc.l	$0000CAB6
		dc.w	$FFF4,$000E ; move.b #$E,($FFFFA48).w
		dc.w	$FFFD,$0E00,$0100,$0101,$0E00,$0000,$1100,$0100
		dc.w	$FFFD,$0F20,$0100,$0108,$0E00,$0000,$1100,$0100
		dc.w	$FFF8,$1100,$0000,$0101
		dc.w	$FFF9,$1100,$0000,$0013,$B5DA,$FFFF,$0000,$3000,$0800
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF9,$1100,$0000,$0013,$C08A,$FFFF,$0000,$4000,$0C00
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF9,$1100,$0000,$0013,$CD04,$FFFF,$0000,$5800,$0400
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$01B0,$1260,$0080,$0000,$0000,$0000,$B000
		dc.w	$FFFF
		dc.w	$FFF7,$1100,$0080,$FFFF,$A158,$0000,$0000
		dc.w	$0014,$1100,$0000,$0000,$0090,$0090,$B001
		dc.w	$FFFF
		dc.w	$FFF7,$1100,$0000,$FFFF,$A0F8,$0000,$0000
		dc.w	$FFF7,$1100,$0000,$FFFF,$A0FC,$0000,$0000
		dc.w	$FFF9,$1100,$0000,$0013,$D2D8,$FFFF,$0000,$2800,$0400
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF9,$1100,$0000,$0013,$D85A,$FFFF,$0000,$3000,$0400
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFFC,$1100,$0000,$001E,$470C,$FFFF,$0000
		dc.w	$FFF6
		dc.w	$FFFD,$1100,$0000,$0101,$1100,$0000,$1380,$0000
		dc.w	$FFF7,$1100,$0000,$FFFF,$A234,$0007,$BB5E
		dc.w	$FFEF,$1100,$0000,$FFFF,$A4F6,$00A8
		dc.w	$FFEF,$1100,$0000,$FFFF,$A4F8,$01A0
		dc.w	$001E,$1100,$0000,$0000,$01F8,$00D8,$B001
		dc.w	$001F,$1100,$0000,$0000,$01C8,$00D8,$B001
		dc.w	$012B,$1100,$0000,$0001,$1328,$0080,$B001
		dc.w	$012B,$1100,$0000,$0001,$13E8,$0088,$B001
		dc.w	$012E,$1200,$0000,$0001,$14C0,$0090,$B001
		dc.w	$FFFB,$12C0,$0000,$FFFF,$0000,$E000,$0800
		dc.w	$FFF5
		dc.w	$FFF7,$12C0,$0000,$FFFF,$A18C,$0001,$75F2
		dc.w	$FFF7,$12C0,$0000,$FFFF,$A190,$0000,$7858
		dc.w	$FFF7,$1380,$0000,$FFFF,$A158,$0001,$783A
		dc.w	$FFEF,$1380,$0000,$FFFF,$A240,$0001,$FFFF
		dc.w	$FFF7,$1D80,$0100,$FFFF,$A234,$0007,$BADC
		dc.w	$FFFD,$1D80,$0100,$0101,$1D80,$0100,$1EC0,$0100
		dc.w	$FFF0,$1D80,$0100,$00FC
		dc.w	$009D,$1F20,$0100,$0001,$1F60,$01CA,$B001
		dc.w	$FFF9,$1EC0,$0100,$0016,$C906,$FFFF,$0000,$3800,$0400
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF9,$1EC0,$0100,$0013,$DE10,$FFFF,$0000,$4000,$1000
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF9,$1EC0,$0100,$0013,$F288,$FFFF,$0000,$6000,$0800
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF9,$1EC0,$0100,$0013,$FD98,$FFFF,$0000,$7000,$0800
		dc.w	$FFF6
		dc.w	$FFF5
		dc.w	$FFF0,$1EC0,$0100,$00C1
		dc.w	$009C,$2020,$0100,$0000,$0120,$FFFC,$B081
		dc.w	$FFFF
		dc.w	$01D1,$2020,$0100,$0000,$0000,$0000,$B001
		dc.w	$FFFF
		dc.w	$FFF3,$0000
		dc.w	$FFF2
		dc.w	$FFF1
		dc.w	$FFFF
 endif
 
 if endofrom = 1
 
Level0C_ObjectList:

		; goto lev 0 test demo and skip this
		;LevAlterCheckpoint $00                                         ; ROM : 0x0007E462
        ;LevExecPalEffect   $0000                                       ; ROM : 0x0007E6CE
        ;LevWaitPalEffectEnd                                            ; ROM : 0x0007E6D2
        ;LevEnd                                                         ; ROM : 0x0007E6D4

        LevScrollSetEx     $0000,$0100,$0000,$0000,$0E00,$0100,player,L2R   ; ROM : 0x0007E1D8
        LevObj             ObjID_Lev1ArmoredCarrier,		$0000,$0100,$0000,$0090,$0090,$B001   ; ROM : 0x0007E1E8
        LevObj             ObjID_Lev1CendepideIntro,		$0000,$0100,$0000,$0000,$0000,$B081   ; ROM : 0x0007E1F6
        LevObj             ObjID_Lev1AgroRobotBombIntro,	$0000,$0100,$0000,$0120,$0148,$B001   ; ROM : 0x0007E204
        LevObj             ObjID_Lev1AgroRobotBombIntro,	$0000,$0100,$0000,$0168,$0148,$B001   ; ROM : 0x0007E204
        LevObj             ObjID_Lev1AgroRobotSlashIntro,	$0000,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E212
        LevObj             ObjID_Lev1AgroRobotSlashIntro,	$0180,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E220
        LevObj             ObjID_Lev1AgroRobotShotIntro,	$0200,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E22E
		
        LevObj             ObjID_WeaponBirdC,				$03B0,$0100,$0000,$0050,$00E0,$B001   ; ROM : 0x0007E290
        LevObj             ObjID_WeaponBirdA,				$03B0,$0100,$0000,$0080,$00F0,$B001   ; ROM : 0x0007E29E
        LevObj             ObjID_Lev1AgroRobotBomb,			$03D0,$0100,$0000,$01C8,$0118,$B001   ; ROM : 0x0007E2AC
        LevObj             ObjID_Lev1AgroRobotSniper,		$0448,$0100,$0000,$01C8,$00E0,$B001   ; ROM : 0x0007E258
        LevObj             ObjID_Lev1AgroRobotSniper,		$0448,$0100,$0000,$01C8,$0100,$B001   ; ROM : 0x0007E258
        LevObj             ObjID_Lev1AgroRobotBomb,			$04C0,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E2AC
        LevObj             ObjID_Lev1AgroRobotBomb,			$0500,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E2AC

        LevObj             ObjID_Lev1AgroRobotSpawner,		$0540,$0100,$0000,$0A00,$0000,$B001   ; ROM : 0x0007E24A
        LevObj             ObjID_Lev1AgroRobotSniper,		$05E0,$0100,$0001,$05C0,$0161,$B001   ; ROM : 0x0007E258
        LevObj             ObjID_Lev1AgroRobotBomb,			$0620,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E2AC
        LevObj             ObjID_Lev1AgroRobotSniper,		$0650,$0100,$0001,$0650,$0161,$B001   ; ROM : 0x0007E266
        LevObj             ObjID_Lev1AgroRobotBomb,			$06E0,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E2AC
        ;LevObj             ObjID_Lev1AgroRobotBomb,			$0760,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E2AC
        LevObj             ObjID_Lev1AgroRobotBomb,			$07B0,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E2AC
        LevObj             ObjID_Lev1AgroRobotSniperExp1,	$0814,$0100,$0001,$0814,$0169,$B001   ; ROM : 0x0007E274
        LevObj             ObjID_Lev1AgroRobotBomb,			$0860,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E2AC
        LevObj             ObjID_Lev1AgroRobotSniperExp2,	$0934,$0100,$0001,$0934,$0169,$B001   ; ROM : 0x0007E282
        ;LevObj             ObjID_Lev1AgroRobotBomb,			$09C0,$0100,$0001,$09C0,$01AE,$B001   ; ROM : 0x0007E2AC
        LevObj             ObjID_Lev1AgroRobotBomb,			$0A00,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E2AC
        
		; ambush new
        LevObj             ObjID_Lev1AgroRobotSpawner,		$0B00,$0100,$0000,$0B90,$0500,$B001   ; ROM : 0x0007E24A
		
		; ambush old
		;LevObj             ObjID_Lev1AgroRobotSlash,		$0B90,$0100,$0000,$0198,$0148,$B001   ; ROM : 0x0007E212
        ;LevObj             ObjID_Lev1AgroRobotSlash,		$0B90,$0100,$0000,$01A0,$0148,$B001   ; ROM : 0x0007E212
        ;LevObj             ObjID_Lev1AgroRobotSlash,		$0B90,$0100,$0000,$01A8,$0148,$B001   ; ROM : 0x0007E212
        ;LevObj             ObjID_Lev1AgroRobotSlash,		$0B90,$0100,$0000,$01B0,$0148,$B001   ; ROM : 0x0007E212
        ;LevObj             ObjID_Lev1AgroRobotSlash,		$0B90,$0100,$0000,$01B8,$0148,$B001   ; ROM : 0x0007E220
        ;LevObj             ObjID_Lev1AgroRobotSlash,		$0B90,$0100,$0000,$01C0,$0148,$B001   ; ROM : 0x0007E220
        ;LevObj             ObjID_Lev1AgroRobotSlash,		$0B90,$0100,$0000,$01C8,$0148,$B001   ; ROM : 0x0007E220
		
        ;LevObj             ObjID_Lev1GasolineTanker,		$0A00,$0100,$0001,$0A00,$0100,$B201   ; ROM : 0x0007E2BA
        ;LevObj             ObjID_Lev1AgroRobotSniper,		$0AF8,$0100,$0001,$0AF8,$0182,$B001   ; ROM : 0x0007E2C8
        ;LevWaitEvent                                                   ; ROM : 0x0007E2D6
        ;LevScrollSetEx     $0000,$0100,$0000,$0000,$0E00,$0100,player,L2R   ; ROM : 0x0007E2D8
		
        LevObj             ObjID_Lev1CendepideSpawner		,$0C30,$0100,$0000,$0000,$0000,$B001   ; ROM : 0x0007E2E8
        LevObj             ObjID_Lev1Earthquake				,$0C50,$0100,$0000,$0000,$0000,$B001   ; ROM : 0x0007E2F6
        LevExec            $0C00,$0100,$0000C996                       ; ROM : 0x0007E304
        LevObj             ObjID_WeaponBirdE				,$0E80,$0100,$0000,$01D0,$0100,$B301   ; ROM : 0x0007E30E
        
		LevExec            $0DE0,$0100,$000067DE                       ; ROM : 0x0007E31C
        LevLoadGFX         $0E00,$0100,$00139904,$FFFF0000,$3000,$0800 ; ROM : 0x0007E326
        LevWaitLZKN                                                    ; ROM : 0x0007E338
        LevWaitVDPLoad                                                 ; ROM : 0x0007E33A
        LevLoadGFX         $0E00,$0100,$0013A252,$FFFF0000,$4000,$1000 ; ROM : 0x0007E33C
        LevWaitLZKN                                                    ; ROM : 0x0007E34E
        LevWaitVDPLoad                                                 ; ROM : 0x0007E350
        LevLoadGFX         $0E00,$0100,$0013B0EE,$FFFF0000,$7800,$0400 ; ROM : 0x0007E352
        LevWaitLZKN                                                    ; ROM : 0x0007E364
        LevWaitVDPLoad                                                 ; ROM : 0x0007E366
        LevWriteValue      $0E00,$0100,$FFFFA18C,$00017698             ; ROM : 0x0007E368
        LevWriteValue      $0E00,$0100,$FFFFA190,$00000000             ; ROM : 0x0007E376
        LevUncLZKN         $0E00,$0100,$001E4AB6,$FFFF0000             ; ROM : 0x0007E384
        LevWaitLZKN                                                    ; ROM : 0x0007E392
        LevMoveToVRAM      $0E00,$0100,$FFFF0000,$E000,$0800           ; ROM : 0x0007E394
        LevWaitVDPLoad                                                 ; ROM : 0x0007E3A2
        LevObj             ObjID_Lev1Spider					,$0F60,$0100,$0000,$0120,$0130,$B000   ; ROM : 0x0007E3A4
        LevWaitEvent                                                   ; ROM : 0x0007E3B2
        LevObj             ObjID_Lev1BuildingWindowsSpawner	,$0E00,$0100,$0000,$0090,$0090,$C380   ; ROM : 0x0007E3B4
        LevScrollSetEx     $0E00,$0100,$0E00,$0000,$0EC0,$0100,player,L2R   ; ROM : 0x0007E3C2
        LevExec            $0E00,$0100,$000177CE                       ; ROM : 0x0007E3D2
        LevExec            $0EC0,$0100,$00017800                       ; ROM : 0x0007E3DC
        LevWriteValue      $0EC0,$0100,$FFFFA18C,$000176E8             ; ROM : 0x0007E3E6
        LevWriteValue      $0EC0,$0100,$FFFFA190,$00007858             ; ROM : 0x0007E3F4
        LevExec            $0EC0,$0100,$00017800                       ; ROM : 0x0007E402
        LevExecWhileTrue   $0EC0,$0100,$00017820                       ; ROM : 0x0007E40C
        LevObj             ObjID_Lev1EventMaster,$0EC0,$0100,$0000,$0090,$0090,$B001   ; ROM : 0x0007E416
        LevWaitEvent                                                   ; ROM : 0x0007E424
        
		LevWriteByte       $0EC0,$0100,$FFFFA34E,$01                 ; ROM : 0x0007E426
        LevExec            $0E00,$0100,$00017592                       ; ROM : 0x0007E432
        LevWriteValue      $0E00,$0100,$FFFFA18C,$00000000             ; ROM : 0x0007E43C
        LevWriteValue      $0E00,$0100,$FFFFA190,$00000000             ; ROM : 0x0007E44A
        LevExec            $0E00,$0100,$0000CAB6                       ; ROM : 0x0007E458
        
		;--------------------------------------------------------------------------------
		
		LevAlterCheckpoint $0E                                         ; ROM : 0x0007E462
        LevScrollSetEx     $0E00,$0100,$0E00,$0000,$1100,$0100,player,L2R   ; ROM : 0x0007E466
        LevScrollSetEx     $0F20,$0100,$0E00,$0000,$1100,$0100,player,LB2RT   ; ROM : 0x0007E476
        LevScrollAlterEx   $1100,$0000,player,L2R                           ; ROM : 0x0007E486
        LevLoadGFX         $1100,$0000,$0013B5DA,$FFFF0000,$3000,$0800 ; ROM : 0x0007E48E
        LevWaitLZKN                                                    ; ROM : 0x0007E4A0
        LevWaitVDPLoad                                                 ; ROM : 0x0007E4A2
        LevLoadGFX         $1100,$0000,$0013C08A,$FFFF0000,$4000,$0C00 ; ROM : 0x0007E4A4
        LevWaitLZKN                                                    ; ROM : 0x0007E4B6
        LevWaitVDPLoad                                                 ; ROM : 0x0007E4B8
        LevLoadGFX         $1100,$0000,$0013CD04,$FFFF0000,$5800,$0400 ; ROM : 0x0007E4BA
        LevWaitLZKN                                                    ; ROM : 0x0007E4CC
        LevWaitVDPLoad                                                 ; ROM : 0x0007E4CE
		
        LevObj             ObjID_Lev1GiantTiny					,$1260,$0080,$0000,$0000,$0000,$B000   ; ROM : 0x0007E4D0
        LevWaitEvent                                                   ; ROM : 0x0007E4DE
        LevWriteValue      $1100,$0080,$FFFFA158,$00000000             ; ROM : 0x0007E4E0
        LevObj             ObjID_Lev1EventMaster	,$1100,$0000,$0000,$0090,$0090,$B001   ; ROM : 0x0007E4EE
        LevWaitEvent                                                   ; ROM : 0x0007E4FC
		
        LevWriteValue      $1100,$0000,$FFFFA0F8,$00000000             ; ROM : 0x0007E4FE
        LevWriteValue      $1100,$0000,$FFFFA0FC,$00000000             ; ROM : 0x0007E50C
		
        ;���������� �������
        LevLoadGFX         $1100,$0000,$00134C08,$FFFF0000,$2000,$1000 ; ROM : 0x0007E51A
        LevWaitLZKN                                                    ; ROM : 0x0007E52C
        LevWaitVDPLoad                                                 ; ROM : 0x0007E52E
        LevLoadGFX         $1100,$0000,$00135ED8,$FFFF0000,$4000,$1000 ; ROM : 0x0007E51A
        LevWaitLZKN                                                    ; ROM : 0x0007E52C
        LevWaitVDPLoad                                                 ; ROM : 0x0007E52E
		
        LevLoadGFX         $1100,$0000,$0013D2D8,$FFFF0000,$2800,$0400 ; ROM : 0x0007E51A
        LevWaitLZKN                                                    ; ROM : 0x0007E52C
        LevWaitVDPLoad                                                 ; ROM : 0x0007E52E
        LevLoadGFX         $1100,$0000,$0013D85A,$FFFF0000,$3000,$0400 ; ROM : 0x0007E530
        LevWaitLZKN                                                    ; ROM : 0x0007E542
        LevWaitVDPLoad                                                 ; ROM : 0x0007E544
        ;LevUncLZKN         $1100,$0000,$001E470C,$FFFF0000             ; ROM : 0x0007E546
        ;LevWaitLZKN                                                    ; ROM : 0x0007E554
		
        LevScrollSetEx     $1100,$0000,$1100,$0000,$1200,$0020,player,LT2RB   ; ROM : 0x0007E556
        ;LevScrollSetEx     $1100,$0020,$1100,$0020,$11C0,$0040,player,LT2RB   ; ROM : 0x0007E556
        ;LevScrollSetEx     $11C0,$0040,$11C0,$0040,$1200,$0060,player,LT2RB   ; ROM : 0x0007E556
        LevScrollSetEx     $11C0,$0020,$1100,$0020,$1300,$0100,player,LT2RB   ; ROM : 0x0007E556
		
        ;LevWriteValue      $1100,$0000,$FFFFA234,$0007BB5E             ; ROM : 0x0007E566
        LevWriteWord       $1100,$0000,$FFFFA4F6,$00A8                 ; ROM : 0x0007E574
        LevWriteWord       $1100,$0000,$FFFFA4F8,$01A0                 ; ROM : 0x0007E580
        LevObj             ObjID_Lev1RotationStickRobo		,$1100,$0060,$0001,$1358,$00B0,$B001   ; ROM : 0x0007E5A8
        LevObj             ObjID_Lev1RotationStickRobo		,$1100,$0060,$0001,$1388,$0140,$B001   ; ROM : 0x0007E5A8
		LevWriteValue      $12A0,$0060,$FFFFA178,$13400000
        ;LevScrollSetEx     $1200,$0020,$1100,$0000,$1280,$0040,player,LT2RB   ; ROM : 0x0007E556
        ;;LevScrollSetEx     $1280,$0060,$1280,$0000,$12C0,$0100,player,LT2RB   ; ROM : 0x0007E556
		;LevWriteValue      $1298,$0020,$FFFFA17C,$00400000
		;LevLoadScript      $1298,$0020,$0000,$00020D60
        ;LevScrollSetEx     $1298,$0020,$1100,$0000,$1380,$0040,autocam,L2R   ; ROM : 0x0007E556
        ;LevScrollSetEx     $12C0,$0100,$1100,$0000,$1EC0,$0100,player,LT2RB   ; ROM : 0x0007E556
        ;LevScrollSetEx     $1340,$0100,$1100,$0000,$1EC0,$0100,player,L2R   ; ROM : 0x0007E556
        LevScrollSetEx     $1300,$0100,$1300,$0100,$1680,$0100,player,L2R   ; ROM : 0x0007E556
        LevObj             ObjID_WeaponBirdD,$1300,$0100,$0000,$01F8,$00D8,$B001   ; ROM : 0x0007E58C
        LevObj             ObjID_WeaponBirdB,$1300,$0100,$0000,$01C8,$00D8,$B001   ; ROM : 0x0007E59A
        LevObj             ObjID_Lev1AgroRobotSpawner,		$1500,$0100,$0000,$15E0,$1000,$B001   ; ROM : 0x0007E24A
		
		; ������ �����������
        LevLoadGFX         $1600,$0100,$0013CD04,$FFFF0000,$5800,$0400 ; ROM : 0x0007E4BA
        LevWaitLZKN                                                    ; ROM : 0x0007E4CC
        LevWaitVDPLoad                                                 ; ROM : 0x0007E4CE
        LevUncLZKN         $1600,$0100,$001E470C,$FFFF0000             ; ROM : 0x0007E546
        LevWaitLZKN                                                    ; ROM : 0x0007E554

        LevObj             ObjID_Lev1RotationStickAgroRobo,$1680,$0100,$0001,$17C0,$0180,$B001   ; ROM : 0x0007E5C4
		
        ;LevObj             ObjID_Lev1RotationStickRobo,$1680,$0100,$0001,$17C0,$0180,$B001   ; ROM : 0x0007E5C4
        ;LevWriteValue      $1680,$0100,$FFFFA18C,$000175F2             ; ROM : 0x0007E5E2
        ;LevWriteValue      $1680,$0100,$FFFFA190,$00007858             ; ROM : 0x0007E5F0
        ;LevWriteValue      $16C0,$0100,$FFFFA158,$0001783A             ; ROM : 0x0007E5FE
        LevWriteWord       $1680,$0100,$FFFFA240,$0001                 ; ROM : 0x0007E60C
        LevMoveToVRAM      $1680,$0000,$FFFF0000,$E000,$0800           ; ROM : 0x0007E5D2
        LevWaitVDPLoad                                                 ; ROM : 0x0007E5E0
        LevWriteValue      $1680,$0000,$FFFFA18C,$000175F2             ; ROM : 0x0007E5E2
        LevWriteValue      $1680,$0000,$FFFFA190,$00007858             ; ROM : 0x0007E5F0
        LevWriteValue      $16C0,$0000,$FFFFA158,$0001783A             ; ROM : 0x0007E5FE
        LevWriteWord       $16C0,$0000,$FFFFA240,$0001                 ; ROM : 0x0007E60C
        LevWaitEvent                                                   ; ROM : 0x0007E618
        ;LevScrollSetEx     $1D80,$0100,$1D80,$0100,$1EC0,$0100,player,None   ; ROM : 0x0007E628
        LevScrollSet       $1D80,$0100,$0101,$1D80,$0100,$1EC0,$0100   ; ROM : 0x0007E628
        LevPlaySound       $1D80,$0100,$00FC                           ; ROM : 0x0007E638
		
		; remove all stick robo's
		;LevExec            $1D80,$0100,Level0C_DeleteSticks
		
        LevObj             $009D,$1F20,$0100,$0001,$1F60,$01CA,$B001   ; ROM : 0x0007E640
        LevLoadGFX         $1EC0,$0100,$0016C906,$FFFF0000,$3800,$0400 ; ROM : 0x0007E64E
        LevWaitLZKN                                                    ; ROM : 0x0007E660
        LevWaitVDPLoad                                                 ; ROM : 0x0007E662
        LevLoadGFX         $1EC0,$0100,$0013DE10,$FFFF0000,$4000,$1000 ; ROM : 0x0007E664
        LevWaitLZKN                                                    ; ROM : 0x0007E676
        LevWaitVDPLoad                                                 ; ROM : 0x0007E678
        LevLoadGFX         $1EC0,$0100,$0013F288,$FFFF0000,$6000,$0800 ; ROM : 0x0007E67A
        LevWaitLZKN                                                    ; ROM : 0x0007E68C
        LevWaitVDPLoad                                                 ; ROM : 0x0007E68E
        LevLoadGFX         $1EC0,$0100,$0013FD98,$FFFF0000,$7000,$0800 ; ROM : 0x0007E690
        LevWaitLZKN                                                    ; ROM : 0x0007E6A2
        LevWaitVDPLoad                                                 ; ROM : 0x0007E6A4
        LevPlaySound       $1EC0,$0100,$00C1                           ; ROM : 0x0007E6A6
        LevObj             $009C,$2020,$0100,$0000,$0120,$FFFC,$B081   ; ROM : 0x0007E6AE
        LevWaitEvent                                                   ; ROM : 0x0007E6BC
        LevObj             $01D1,$2020,$0100,$0000,$0000,$0000,$B001   ; ROM : 0x0007E6BE
        LevWaitEvent                                                   ; ROM : 0x0007E6CC
        LevExecPalEffect   $0000                                       ; ROM : 0x0007E6CE
        LevWaitPalEffectEnd                                            ; ROM : 0x0007E6D2
        LevEnd                                                         ; ROM : 0x0007E6D4


Level1Map:
		binclude	"SuperHardCorps\Level\Level1Map.cmp.bin"
		align 2
		
Level0C_CollList:
		dc.w $0000,$03C4
		dc.w $0004,$03F7
		dc.w $0000,$04A9
		dc.w $0004,$04D9
		dc.w $0005,$04DD
		dc.w $0001,$04DF
		dc.w $0007,$04E7
		dc.w $0008,$04EE
		dc.w $0009,$04F1
		dc.w $000A,$04F4
		dc.w $0006,$0500
		dc.w $0000,$7FFF

		; original
		;dc.w $0000,$04A9
		;dc.w $0004,$04D9
		;dc.w $0005,$04DD
		;dc.w $0001,$04DF
		;dc.w $0007,$04E7
		;dc.w $0008,$04EE
		;dc.w $0009,$04F1
		;dc.w $000A,$04F4
		;dc.w $0006,$0500
		;dc.w $0000,$7FFF
		
		;dc.b	"ohmagad!"
Level0C_DeleteSticks:
		movem.l	d0-d4/a6,-(sp)
		moveq   #0,d1
		moveq   #0,d2
		moveq   #0,d3
		moveq   #0,d4
		lea     ($FFFFAF80).w,a6
		move.w  #$4D,d0 ; 'M'

Level0C_DeleteSticks_loop:
		lea     $80(a6),a6
		
		cmp.w   #ObjID_Lev1RotationStickRobo,(a6)
		beq		Level0C_DeleteSticks_kill
		cmp.w   #ObjID_Lev1RotationStickRoboPart,(a6)
		beq		Level0C_DeleteSticks_kill
		;cmp.w   #ObjID_Lev1RotationStickAgroRobo,(a6)
		;beq		Level0C_DeleteSticks_kill
		
		dbra    d0,Level0C_DeleteSticks_loop
		movem.l	(sp)+,d0-d4/a6
		rts
		
Level0C_DeleteSticks_kill:
		lea     $80(a6),a6
		movem.l d1-d4,-(a6)
		movem.l d1-d4,-(a6)
		movem.l d1-d4,-(a6)
		movem.l d1-d4,-(a6)
		movem.l d1-d4,-(a6)
		movem.l d1-d4,-(a6)
		movem.l d1-d4,-(a6)
		movem.l d1-d4,-(a6)
		bra		Level0C_DeleteSticks_loop
		
 endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
