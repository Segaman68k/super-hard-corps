;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0
		org			$7BC1A
		dc.l		Level01_ObjectList
 
 endif
 
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

		;org		$7D7EA
Level01_ObjectList:

        LevExec            $0000,$0000,$0000C996                       ; ROM : 0x0007D7EA
        LevLoadScript      $00A0,$0070,$0000,$00016976                 ; ROM : 0x0007D7F4
        LevLoadScript      $00A0,$0070,$0001,$000169F4                 ; ROM : 0x0007D800
        LevObj             $001D,$0000,$0000,$0000,$01C8,$0100,$0004   ; ROM : 0x0007D80C
        LevObj             $001F,$0000,$0000,$0000,$0060,$0100,$0004   ; ROM : 0x0007D81A
        LevObj             $00A0,$0160,$0000,$0000,$0000,$0000,$B000   ; ROM : 0x0007D828
        LevWaitEvent                                                   ; ROM : 0x0007D836
        LevScrollSet       $0000,$0000,$0101,$0000,$0000,$06C0,$01FF   ; ROM : 0x0007D838
        LevObj             $0082,$0180,$0000,$0000,$01F0,$0140,$B001   ; ROM : 0x0007D848
        LevObj             $0082,$01C0,$0000,$0000,$01F0,$0140,$B001   ; ROM : 0x0007D856
        LevObj             $0082,$0200,$0000,$0000,$01F0,$0140,$B001   ; ROM : 0x0007D864
        LevObj             $0082,$0280,$0000,$0000,$01F0,$0140,$B001   ; ROM : 0x0007D872
        LevObj             $0082,$02A0,$0000,$0000,$01F0,$0140,$B001   ; ROM : 0x0007D880
        LevObj             $0082,$02C0,$0000,$0000,$01F0,$0140,$B001   ; ROM : 0x0007D88E
        LevObj             $0082,$02E0,$0000,$0000,$0060,$0140,$B001   ; ROM : 0x0007D89C
        LevObj             $0082,$0300,$0000,$0000,$01F0,$0140,$B001   ; ROM : 0x0007D8AA
        LevObj             $0081,$03B0,$0000,$0001,$03D0,$00A8,$B001   ; ROM : 0x0007D8B8
        LevObj             $0082,$03C0,$0000,$0000,$0060,$0140,$B001   ; ROM : 0x0007D8C6
        LevObj             $0082,$03D0,$0000,$0000,$0060,$0140,$B001   ; ROM : 0x0007D8D4
        LevObj             $0082,$0400,$0000,$0000,$01A0,$0170,$B001   ; ROM : 0x0007D8E2
        LevObj             $0082,$0420,$0000,$0000,$0060,$0140,$B001   ; ROM : 0x0007D8F0
        LevObj             $0082,$0440,$0000,$0000,$01F0,$0140,$B001   ; ROM : 0x0007D8FE
        LevObj             $0082,$0480,$0000,$0000,$0190,$0170,$B001   ; ROM : 0x0007D90C
        LevObj             $0082,$0500,$0000,$0000,$01A0,$0170,$B001   ; ROM : 0x0007D91A
        LevObj             $0082,$0500,$0000,$0000,$00A0,$0190,$B001   ; ROM : 0x0007D928
        LevObj             $0082,$0510,$0000,$0000,$01C0,$0170,$B001   ; ROM : 0x0007D936
        LevObj             $0081,$0540,$0000,$0001,$0580,$00A8,$B001   ; ROM : 0x0007D944
        LevObj             $0082,$0540,$0000,$0000,$00A0,$0170,$B001   ; ROM : 0x0007D952
        LevObj             $0082,$0560,$0000,$0000,$00A0,$0170,$B001   ; ROM : 0x0007D960
        LevObj             $0082,$0570,$0000,$0000,$0140,$0170,$B001   ; ROM : 0x0007D96E
        LevExec            $03C0,$0000,$0000C9BA                       ; ROM : 0x0007D97C
        LevPlaySound       $0600,$0000,$00FD                           ; ROM : 0x0007D986
        LevObj             $0100,$0580,$0000,$0001,$06C0,$00BC,$B001   ; ROM : 0x0007D98E
        LevObj             $0100,$0620,$0000,$0001,$0760,$00BC,$B001   ; ROM : 0x0007D99C
        LevObj             $0100,$06C0,$0000,$0001,$0800,$00BC,$B001   ; ROM : 0x0007D9AA
        LevObj             $0021,$06C0,$0000,$0000,$01C8,$0100,$B001   ; ROM : 0x0007D9B8
        LevLoadGFX         $06C0,$0000,$0015CEBC,$FFFF0000,$2000,$1000 ; ROM : 0x0007D9C6
        LevWaitLZKN                                                    ; ROM : 0x0007D9D8
        LevWaitVDPLoad                                                 ; ROM : 0x0007D9DA
        LevLoadGFX         $06C0,$0000,$0015E5B4,$FFFF0000,$4000,$1000 ; ROM : 0x0007D9DC
        LevWaitLZKN                                                    ; ROM : 0x0007D9EE
        LevWaitVDPLoad                                                 ; ROM : 0x0007D9F0
        LevWriteValue      $06C0,$0000,$FFFFA18C,$000163C0             ; ROM : 0x0007D9F2
        LevUncLZKN         $06C0,$0000,$001DF7A0,$FFFF0000             ; ROM : 0x0007DA00
        LevWaitLZKN                                                    ; ROM : 0x0007DA0E
        LevMoveToVRAM      $06C0,$0000,$FFFF0000,$E000,$03C0           ; ROM : 0x0007DA10
        LevWaitVDPLoad                                                 ; ROM : 0x0007DA1E
        LevLoadScript      $06C0,$0000,$0002,$00016ACE                 ; ROM : 0x0007DA20
        LevPlaySound       $06C0,$0000,$00C0                           ; ROM : 0x0007DA2C
        LevObj             $0101,$081F,$0000,$0001,$0760,$0070,$B001   ; ROM : 0x0007DA34
        LevWaitEvent                                                   ; ROM : 0x0007DA42
        LevLoadGFX         $06C0,$0000,$0015F828,$FFFF0000,$3000,$0800 ; ROM : 0x0007DA44
        LevWaitLZKN                                                    ; ROM : 0x0007DA56
        LevWaitVDPLoad                                                 ; ROM : 0x0007DA58
        LevLoadGFX         $06C0,$0000,$0015FFFA,$FFFF0000,$4000,$1000 ; ROM : 0x0007DA5A
        LevWaitLZKN                                                    ; ROM : 0x0007DA6C
        LevWaitVDPLoad                                                 ; ROM : 0x0007DA6E
        LevScrollSet       $06C0,$0000,$0101,$0000,$0000,$0C60,$01FF   ; ROM : 0x0007DA70
        LevPlaySound       $06C0,$0000,$00B8                           ; ROM : 0x0007DA80
        LevScrollAlter     $06C0,$0000,$0102                           ; ROM : 0x0007DA88
        LevObj             $0080,$0AC0,$0160,$0001,$0AD0,$01B0,$B001   ; ROM : 0x0007DA90
        LevObj             $0080,$0B00,$0180,$0001,$0B10,$01D0,$B001   ; ROM : 0x0007DA9E
        LevObj             $0080,$0BE0,$0200,$0001,$0BF0,$0250,$B001   ; ROM : 0x0007DAAC
        LevObj             $0080,$0BE0,$0220,$0001,$0BF0,$0270,$B001   ; ROM : 0x0007DABA
        LevObj             $0080,$0CC0,$0220,$0001,$0CD0,$0270,$B001   ; ROM : 0x0007DAC8
        LevObj             $0080,$0D00,$0220,$0001,$0D10,$0250,$B001   ; ROM : 0x0007DAD6
        LevObj             $0080,$0D40,$0220,$0001,$0D50,$0270,$B001   ; ROM : 0x0007DAE4
        ;LevObj             $0046,$0D88,$02C8,$0001,$0D70,$0220,$B001   ; ROM : 0x0007DAF2
        LevObj             $002A,$0D88,$02C8,$0001,$0D90,$02C8,$B001   ; ROM : 0x0007DB00
        LevWriteWord       $0C00,$0000,$FFFFA4F6,$00B8                 ; ROM : 0x0007DB0E
        LevPlaySound       $0C00,$0000,$00C6                           ; ROM : 0x0007D986
        LevScrollSetEx     $0C60,$01FF,$0BB0,$0000,$0D67,$01FF,player,Free   ; ROM : 0x0007DA70
        LevWaitEvent                                                   ; ROM : 0x0007DB1A
        ;LevScrollSet       $06C0,$0000,$0101,$0000,$0000,$0D67,$01FF   ; ROM : 0x0007DB1C
        ;LevObj             $000F,$0E90,$02C8,$0001,$0E98,$02C8,$C001   ; ROM : 0x0007DB2C
        ;LevWaitEvent                                                   ; ROM : 0x0007DB3A
        ;LevAlterCheckpoint $04                                         ; ROM : 0x0007DB3C
        LevPlaySound       $0C00,$0000,$00FD                           ; ROM : 0x0007D986
        LevExecPalEffect   $0000                                       ; ROM : 0x0007DB40
        LevWaitPalEffectEnd                                            ; ROM : 0x0007DB44
		LevExec			   $0C00,$0000,$200
        LevEnd                                                         ; ROM : 0x0007DB46
 
 endif
