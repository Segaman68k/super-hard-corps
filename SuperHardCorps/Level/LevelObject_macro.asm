;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; LEVEL OBJECT MACROS
;
; ������ ������ �������� ��� ������ �������
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Camera move
None		equ $00
L2R			equ $01 ; V $E0
LT2RB		equ $02
T2B			equ $03 ; H $140
RT2LB		equ $04
R2L			equ $05 ; V $E0
RB2LT		equ $06
B2T			equ $07 ; H $140
LB2RT		equ $08
Static		equ $09
Free		equ $0A

player		equ	$01
autocam 	equ	$02
inknow		equ	$03

CamSet macro mode,dir
	( ( (mode&$F)<<8 ) || (dir&$F) )
	endm

; Objects vars
ObjID		equ	$0
ObjSubID	equ	$2
ObjX		equ	$6
ObjY		equ	$A
ObjHSP		equ	$E
ObjVSP		equ	$12
ObjSPR		equ	$16
ObjSPRPAL	equ	$1A
ObjSPRCFG	equ	$1C
ObjANIM		equ	$26
ObjANIMTIMER equ	$2A

; Sprite CFG
;;SprIllegal	equ	$01
SprInv		equ	$02
SprPalSel	equ	$04
SprPrior	equ	$08
SprFlipH	equ	$10
SprFlipV	equ	$20
SprPlayer	equ	$40
SprParticle	equ	$80

;============================================================
; Spawn object ($0001-$7FFF)
;============================================================
LevObj macro ObjID,fromScreenX,fromScreenY,Params,X,Y,SpriteParams

	dc.w ObjID
	dc.w fromScreenX
	dc.w fromScreenY
	dc.w Params
	dc.w X
	dc.w Y
	dc.w SpriteParams
	
 endm

;============================================================
; Load Script
;============================================================
LevLoadScript macro fromScreenX,fromScreenY,Cell,Subroutine

	dc.w $FFED ; -19
	dc.w fromScreenX
	dc.w fromScreenY
	dc.w Cell
	dc.l Subroutine
	
 endm

;============================================================
; Write Byte
;============================================================
LevWriteByte macro fromScreenX,fromScreenY,RAMaddress,Value

	dc.w $FFEE ; -18
	dc.w fromScreenX
	dc.w fromScreenY
	dc.l RAMaddress
	dc.w Value
	
 endm

;============================================================
; Write Word
;============================================================
LevWriteWord macro fromScreenX,fromScreenY,RAMaddress,Value

	dc.w $FFEF ; -17
	dc.w fromScreenX
	dc.w fromScreenY
	dc.l RAMaddress
	dc.w Value
	
 endm

;============================================================
; PlaySound
;============================================================
LevPlaySound macro fromScreenX,fromScreenY,SoundID

	dc.w $FFF0 ; -16
	dc.w fromScreenX
	dc.w fromScreenY
	dc.w SoundID
	
 endm

;============================================================
; LevelEnd
;============================================================
LevEnd macro Value

	dc.w $FFF1 ; -15
	
 endm

;============================================================
; Wait Pal Effect End
;============================================================
LevWaitPalEffectEnd macro Value

	dc.w $FFF2 ; -14
	
 endm

;============================================================
; ExecPalEffect
;============================================================
LevExecPalEffect macro Value

	dc.w $FFF3 ; -13
	dc.w Value
	
 endm

;============================================================
; AlterCheckpoint
;============================================================
LevAlterCheckpoint macro Value

	dc.w $FFF4 ; -12
	dc.w Value
	
 endm

;============================================================
; Wait VDP Load (FFA1CA)
;============================================================
LevWaitVDPLoad macro 

	dc.w $FFF5 ; -11
	
 endm

;============================================================
; Wait LZKN (FFA1B8)
;============================================================
LevWaitLZKN macro 

	dc.w $FFF6 ; -10
	
 endm

;============================================================
; Write Value
;============================================================
LevWriteValue macro fromScreenX,fromScreenY,RAMaddress,Value

	dc.w $FFF7 ; -9
	dc.w fromScreenX
	dc.w fromScreenY
	dc.l RAMaddress
	dc.l Value
	
 endm

;============================================================
; ScrollAlter
;============================================================
LevScrollAlter macro fromScreenX,fromScreenY,Params

	dc.w $FFF8 ; -8
	dc.w fromScreenX
	dc.w fromScreenY
	dc.w Params
	
 endm

;============================================================
; ScrollAlter
;============================================================
LevScrollAlterEx macro fromScreenX,fromScreenY,mode,dir

	dc.w $FFF8 ; -8
	dc.w fromScreenX
	dc.w fromScreenY
	dc.b mode
	dc.b dir
	
 endm

;============================================================
; Load GFX
;============================================================
LevLoadGFX macro fromScreenX,fromScreenY,Archive,RAM,VRAM,ArchSize

	dc.w $FFF9 ; -7
	dc.w fromScreenX
	dc.w fromScreenY
	dc.l Archive
	dc.l RAM
	dc.w VRAM
	dc.w ArchSize
	
 endm

;============================================================
; ExecWhileTrue (FFA0D4)
;============================================================
LevExecWhileTrue macro fromScreenX,fromScreenY,Subroutine

	dc.w $FFFA ; -6
	dc.w fromScreenX
	dc.w fromScreenY
	dc.l Subroutine
	
 endm

;============================================================
; Move To VRAM
;============================================================
LevMoveToVRAM macro fromScreenX,fromScreenY,RAM,VRAM,MemSize

	dc.w $FFFB ; -5
	dc.w fromScreenX
	dc.w fromScreenY
	dc.l RAM
	dc.w VRAM
	dc.w MemSize
	
 endm

;============================================================
; Uncompress LZKN
;============================================================
LevUncLZKN macro fromScreenX,fromScreenY,Archive,RAM

	dc.w $FFFC ; -4
	dc.w fromScreenX
	dc.w fromScreenY
	dc.l Archive
	dc.l RAM
	
 endm

;============================================================
; Set screen scroll settings
;============================================================
LevScrollSet macro fromScreenX,fromScreenY,Params,FromX,FromY,ToX,ToY

	dc.w $FFFD ; -3
	dc.w fromScreenX
	dc.w fromScreenY
	dc.w Params
	dc.w FromX
	dc.w FromY
	dc.w ToX
	dc.w ToY
	
 endm

;============================================================
; Set screen scroll settings
;============================================================
LevScrollSetEx macro fromScreenX,fromScreenY,FromX,FromY,ToX,ToY,mode,dir

	dc.w $FFFD ; -3
	dc.w fromScreenX
	dc.w fromScreenY
	dc.b mode
	dc.b dir
	dc.w FromX
	dc.w FromY
	dc.w ToX
	dc.w ToY
	
 endm

;============================================================
; Execute external code
;============================================================
LevExec macro fromScreenX,fromScreenY,Subroutine

	dc.w $FFFE ; -2
	dc.w fromScreenX
	dc.w fromScreenY
	dc.l Subroutine

 endm
 
;============================================================
; Wait for event ends
;============================================================
LevWaitEvent macro

	dc.w $FFFF ; -1

 endm
 
 ;============================================================
