;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; M A C R O S
; ---------------------------------------------------------------------------
;mTextLine macro x,y
;	dc.b x,y
;	endm

;mTextNextLine macro
;	dc.b $FE
;	endm

;mTextEnd macro
;	dc.b $FF
;	endm

; ---------------------------------------------------------------------------
; �������� ������� �����
; ---------------------------------------------------------------------------
	org     $A296
	lea		(IntroText).l,a0 ; �������� ��������� �� �����
	
	org		$A29E
	jsr		(Intro_PrintText).l ; ������� ��������� ����� ������
	
; ---------------------------------------------------------------------------
; ������� ���������
; ---------------------------------------------------------------------------

	; ������ ����� ���� � ���������
	org		$A2B6
	move.w	#$4,($FFFFA002).w
	
	; ������ ��������� �� ���������
	org		$9D0A+4
	dc.w	$A2B6 - $9D0A
	dc.w	$A2B6 - $9D0A
	dc.w	$A2B6 - $9D0A

	; �������� �������� �� ��������� ������
	org		$98224
 	mTextLine 6,26
	dc.b "@2018 SEGAMAN. NOT FOR SALE!"
	mTextEnd
	
	org		$C14C
	jsr		(Title_PrintText).l
	
 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

; ---------------------------------------------------------------------------
; ����� ������ ������ ������, ���������� ��� �����
; ---------------------------------------------------------------------------

; =============== S U B R O U T I N E =======================================

Title_PrintText:
				moveq		#0,d6
				lea			($00098000).l,a0
				add.w		d7,d7
				move.w		(a0,d7),d7
				adda.w		d7,a0
				bra			sub_0_8A20

Intro_PrintText:
                movem.l		d7/a0-a1,-(sp)
				lea			TextDashTile,a0
				lea			($C00000).l,a1
				moveq		#$10,d7
				VDPSetDest	$2000,4(a1)
-:
				move.w		(a0)+,(a1)
				dbra		d7,-
                movem.l		(sp)+,d7/a0-a1
				
sub_0_8A20:                             ; CODE XREF: ROM:0000A29Ep
                move.w  d6,d4

loc_0_8A22:                             ; CODE XREF: sub_0_8A20+12j
                moveq   #0,d5
                moveq   #0,d6
                move.b  (a0)+,d5
                move.b  (a0)+,d6

loc_0_8A2A:                             ; CODE XREF: sub_0_8A20+2Cj
                moveq   #0,d7
                move.b  (a0)+,d7
                cmpi.b  #-2,d7
                beq.s   loc_0_8A22
                cmpi.b  #-1,d7
                beq.s   locret_0_8A4E
                movem.l d2-d6/a0-a1,-(sp)
                tst.w   d4
                beq.s   loc_0_8A44
                moveq   #0,d7

loc_0_8A44:                             ; CODE XREF: sub_0_8A20+20j
				cmpi.w	#$30,d7
				blt.s	loc_0_8A44_sign
				cmpi.w	#$39,d7
				bgt.s	loc_0_8A44_sign
                addi.w	#$60,d7
				bra.s	loc_0_8A44_print
				
loc_0_8A44_sign:
 				lea		TextConvertList,a1
				
loc_0_8A44_sign_loop:
                move.w  (a1)+,d2
				beq		loc_0_8A44_letter
                move.w  (a1)+,d3
				cmp.w	d7,d2
                bne		loc_0_8A44_sign_loop
				move.w	d3,d7
				bra.s	loc_0_8A44_print

loc_0_8A44_letter:
                addi.w	#$59,d7
				
loc_0_8A44_print:
				jsr   	($8A50).l
                movem.l (sp)+,d2-d6/a0-a1
                addq.w  #1,d5
                bra.s   loc_0_8A2A
; ---------------------------------------------------------------------------

locret_0_8A4E:                          ; CODE XREF: sub_0_8A20+18j
                rts
; End of function sub_0_8A20

; ---------------------------------------------------------------------------
; ������� �����
; ---------------------------------------------------------------------------

TextConvertList:
	dc.w	$20, 0
	dc.w	$21, $EF
	dc.w	$2C, $B4
	dc.w	$2D, $100
	dc.w	$2E, $B5
	dc.w	$40, $B6
	dc.w	0,0

IntroText:
	mTextLine 5,2
	dc.b "CONTRA HARDCORPS TM IS A"
	mTextNextLine
	
	mTextLine 5,4
	dc.b "TRADEMARK OF KONAMI, INC."
	mTextNextLine
	
	mTextLine 5,8
	;dc.b "TM AND @ 1994 KONAMI CO., LTD."
	dc.b "CONTRA SUPER HARDCORPS IS A"
	mTextNextLine
	
	mTextLine 5,10
	dc.b "FAN MODIFICATION HACK."
	mTextNextLine
	
	mTextLine 5,14
	dc.b "GAME DOES NOT HAVE ANY LICENCE"
	mTextNextLine
	
	mTextLine 5,16
	dc.b "AND MADE NOT FOR SALE!"
	mTextNextLine
	
	mTextLine 9,21
	dc.b "HACK BY SEGAMAN @ 2018"
	mTextNextLine
	
	mTextLine 11,23
	dc.b $5E,$5F," PRESENTED BY ",$5E,$5F
	mTextNextLine
	
	mTextLine 5,25
	dc.b "ELEKTROPAGE.RU AND EMU-LAND.NET"
	mTextEnd
	
	;dc.b $01,$08, "SEGAMAN", $FE
	
	; CONTRA HARDSCORPS TM IS A
	;dc.b $05,$08,$9C,$A8,$A7,$AD,$AB,$9A,$00,$A1,$9A,$AB,$9D,$9C,$A8,$AB,$A9,$AC,$00,$AD,$A6,$00,$A2,$AC,$00,$9A,$FE
	; TRADEMARK OF KONAMI, INC.
	;dc.b $05,$0A,$AD,$AB,$9A,$9D,$9E,$A6,$9A,$AB,$A4,$00,$A8,$9F,$00,$A4,$A8,$A7,$9A,$A6,$A2,$B4,$00,$A2,$A7,$9C,$B5,$FE
	; TM AND @ 1994 KONAMI CO., LTD.
	;dc.b $05,$0E,$AD,$A6,$00,$9A,$A7,$9D,$00,$B6,$00,$91,$99,$99,$94,$00,$A4,$A8,$A7,$9A,$A6,$A2,$00,$9C,$A8,$B5,$B4,$00,$A5,$AD,$9D,$B5,$FE
	; LICENSED BY
	;dc.b $0D,$17,$A5,$A2,$9C,$9E,$A7,$AC,$9E,$9D,$00,$9B,$B2,$FE
	; SEGA ENTERPRISES LTD.
	;dc.b $09,$19,$AC,$9E,$A0,$9A,$00,$9E,$A7,$AD,$9E,$AB,$A9,$AB,$A2,$AC,$9E,$AC,$B4,$A5,$AD,$9D,$B5,$FF
	align 2
	
TextDashTile:
	dc.l $00000000 ; 0
	dc.l $00000000 ; 1
	dc.l $00000000 ; 2
	dc.l $FFFFFFF1 ; 3
	dc.l $FFFFFFF1 ; 4
	dc.l $11111111 ; 5
	dc.l $00000000 ; 6
	dc.l $00000000 ; 7

 endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
