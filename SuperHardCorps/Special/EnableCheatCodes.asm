;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 0

; ---------------------------------------------------------------------------
; enable weapon cheat
; ---------------------------------------------------------------------------
	org	$4FC
	jmp	ECC_Weapon
 
; ---------------------------------------------------------------------------
; enable intro cheats
; ---------------------------------------------------------------------------
	org	$C11C
	jmp	ECC_Intro
 
 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; END OF ROM SPACE
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 if endofrom = 1

ECC_Debugger_Counter	equ $FFFFA0E6
ECC_Debugger_Timer		equ $FFFFA0E7

; ---------------------------------------------------------------------------
; enable weapon cheat and debugger cheat
; ---------------------------------------------------------------------------
ECC_Weapon:

	btst    #1,($FFFFFA4C).w
	beq.s   loc_50C
	
	btst    #0,($FFFFA066).w
	bne.s   loc_530

loc_50C:
	jsr		($D344).l
	jsr		(ECC_Debugger).l
	jmp   	$520
	
loc_530:
	jmp		$530
	
; ---------------------------------------------------------------------------

ECC_Debugger:
	tst.b   (ECC_Debugger_Counter).w
	bne.s   ECC_Debugger_2
	move.b  (Joy1pr).w,d0
	andi.b  #$FF,d0
	beq.s   ECC_Debugger_locret
	move.b  #$3C,(ECC_Debugger_Timer).w ; '<'
	addq.b  #1,(ECC_Debugger_Counter).w

ECC_Debugger_2:
	tst.b   (Joy1pr).w
	beq.s   ECC_Debugger_wait
	moveq   #0,d1
	move.b  (ECC_Debugger_Counter).w,d1
	move.b  ECC_Debugger_code(pc,d1.w),d0
	cmp.b   (Joy1pr).w,d0
	bne.s   ECC_Debugger_reset
	addq.b  #1,(ECC_Debugger_Counter).w
	move.b  #$3C,(ECC_Debugger_Timer).w ; '<'
	move.b  ECC_Debugger_code+1(pc,d1.w),d0
	cmpi.b  #$FF,d0
	bne.s   ECC_Debugger_locret
	bra.w   ECC_Debugger_Success
	
; ---------------------------------------------------------------------------

ECC_Debugger_wait:
	subq.b  #1,(ECC_Debugger_Timer).w
	bpl.s   ECC_Debugger_locret

ECC_Debugger_reset:
	clr.b   (ECC_Debugger_Counter).w
	clr.b   (ECC_Debugger_Timer).w

ECC_Debugger_locret:
	rts
; ---------------------------------------------------------------------------

ECC_Debugger_Success:
	jsr		$59730 ; subGetFreeObjectSlot_59730
	bne		ECC_Debugger_Success_error
	
	move.l	#$027F0000,ObjID(a6)
	
	move.w  #$3A,d0 ; laser charge
	jsr     $A8042 ; play sound
	
ECC_Debugger_Success_error:
	rts
	
; ---------------------------------------------------------------------------

ECC_Debugger_code:
	dc.b $FF ; ignore
	dc.b $04 ; l
	dc.b $08 ; r
	dc.b $04 ; l
	dc.b $08 ; r
	dc.b $40 ; a
	dc.b $10 ; b
	dc.b $20 ; c
	dc.b $FF ; end
	align 2

; ---------------------------------------------------------------------------
; enable intro cheats
; ---------------------------------------------------------------------------
ECC_Intro:
	jsr     $59790
	move.w  #$328,(a6)
	
	jsr     $59790
	move.w  #$32B,(a6)
	
	jsr     $59790
	move.w  #$32C,(a6)
	
	jsr     $59790
	move.w  #$3F8,(a6)
	
	lea     ($A59AE).l,a2
	addq.w  #1,($FFFFA004).w
	rts
; ---------------------------------------------------------------------------

 endif

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
