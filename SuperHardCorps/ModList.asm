;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; ALL MODS
;
; ���� ���� �������������, ��� ������ �� ��������� ������ ��������
; ��� ������ �������, ��� � ��� �������, �����.
; �������� ���� � ������� ���������� endofrom
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

modlistcount set modlistcount+1

;============================================================
; ������ �������� ������
;============================================================
 if intro_text
	include SuperHardCorps\IntroText\IntroText.asm
 endif
;============================================================
; ���� �������
;============================================================
 if pal_fix
	include SuperHardCorps\PalFix\PalFix.asm
 endif

;============================================================
; ���� ������
;============================================================
 if damage_fix
	include SuperHardCorps\DamageFix\DamageFix.asm
 endif

;============================================================
; �������� ���-������
;============================================================
 if hit_points
	include SuperHardCorps\HitPoints\HitPoints.asm
 endif

;============================================================
; �������� ���������� Z80 ������
;============================================================
 if z80mod
	include SuperHardCorps\Z80\z80driver.asm
 endif

;============================================================
; �������� ���������� Z80 ������
;============================================================
 if enable_cheat_codes
	include SuperHardCorps\Special\EnableCheatCodes.asm
 endif
;============================================================
; ����� �������� �������
;============================================================

 if LevelList
	include SuperHardCorps\Level\LevelList.asm
 endif

 if Level1ObjectList
	include SuperHardCorps\Level\Level1ObjectList.asm
 endif

 if Level0ObjectList
	include SuperHardCorps\Level\Level0ObjectList.asm
 endif

 if Level4ObjectList
	include SuperHardCorps\Level\Level4ObjectList.asm
 endif

 if Level4Boss
	include SuperHardCorps\Level\Level4Boss.asm
 endif

;============================================================
; ����� ��������
;============================================================

 if Obj0001_Player
	include SuperHardCorps\Objects\Obj0001_Player.asm
 endif

 if Obj002C_Lev1ArmoredCarrier
	include SuperHardCorps\Objects\Obj002C_Lev1ArmoredCarrier.asm
 endif

 if Obj0181_Lev1Cendepide
	include SuperHardCorps\Objects\Obj0181_Lev1Cendepide.asm
 endif

 if Obj009A_Lev1Spider
	include SuperHardCorps\Objects\Obj009A_Lev1Spider.asm
 endif

 if Obj01B5_Lev1Giant
	include SuperHardCorps\Objects\Obj01B5_Lev1Giant.asm
 endif
 
 if Obj009C_Lev1Boss
	include SuperHardCorps\Objects\Obj009C_Lev1Boss.asm
 endif
 
 if Obj0120_121_122_123_Lev1MadRobots
	include SuperHardCorps\Objects\Obj0120_121_122_123_Lev1MadRobots.asm
 endif
 
 if Obj012B_Lev1RotationStickRobo
	include SuperHardCorps\Objects\Obj012B_Lev1RotationStickRobo.asm
 endif
 
 if Obj012A_Lev1AgroRobotSpawner
	include SuperHardCorps\Objects\Obj012A_Lev1AgroRobotSpawner.asm
 endif
 
;============================================================
; Custom objects
;============================================================

 if Obj027F_Debugger
	include SuperHardCorps\Objects\Custom\Obj027F_Debugger.asm
 endif
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
