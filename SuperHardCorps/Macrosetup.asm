;===============================
; Macros
;===============================

VDPSetDest	macro VDPSetDestdest,VDPSetDestreg
		move.l	#$40000000+((((VDPSetDestdest)&$C000)>>14)+(((VDPSetDestdest)&$3FFF)<<16)),VDPSetDestreg
		endm

VDPAddress	macro VDPAddressdest
		;dc.l	$40000000+((((VDPAddressdest)&$C000)>>14)+(((VDPAddressdest)&$3FFF)<<16))
		dc.w	$4000+((VDPAddressdest)&$3FFF)
		dc.w	(((VDPAddressdest)&$C000)>>14)
		endm

;===============================
