bin\asm68k /p /o op+ /o ow+ ScrollFix\ScrollFix.asm,out\ContraHC_GopherFixedScroll.bin
IF NOT EXIST out\ContraHC_GopherFixedScroll.bin goto LABLPAUSE
IF EXIST out\ContraHC_GopherFixedScroll.bin goto Convert2ROM
:Convert2ROM
bin\fixheadr out\ContraHC_GopherFixedScroll.bin
exit /b
:LABLPAUSE

pause


exit /b
