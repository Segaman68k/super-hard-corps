bin\asm68k /p /o op+ /o ow+ HardCore\HardCore.asm,out\ContraHC_HardCore.bin
IF NOT EXIST out\ContraHC_HardCore.bin goto LABLPAUSE
IF EXIST out\ContraHC_HardCore.bin goto Convert2ROM
:Convert2ROM
bin\fixheadr out\ContraHC_HardCore.bin
exit /b
:LABLPAUSE

pause


exit /b
