@echo off

cls
bin\asw  -xx -c -A SuperHardCorps\SuperHardCorps.asm
IF NOT EXIST SuperHardCorps\SuperHardCorps.p goto LABLPAUSE
IF EXIST SuperHardCorps\SuperHardCorps.p goto Convert2ROM
:Convert2ROM
bin\p2bin SuperHardCorps\SuperHardCorps.p out\ContraSuperHardCorps.bin -l 0 -r $-$
bin\rompad.exe out\ContraSuperHardCorps.bin 255 0
bin\fixheadr.exe out\ContraSuperHardCorps.bin
del SuperHardCorps\SuperHardCorps.p
del SuperHardCorps\SuperHardCorps.h
exit /b
:LABLPAUSE

pause


exit /b