 ;CHANGING RESET OF HITPOINTS TO SUBTRACT
 org $11d40
 sub.b   d1,$D(a2)
 
 ;CHANGE LINKS FOR HITPOINTS DRAW
 org $1f0c4
 jmp hitpoints_draw2
 nop
 nop
 jmp hitpoints_draw
 ; nop
 ; nop
 ; rts
 org $1ff000 ; set to empty space to add new code
 ;DRAW HITPOINTS
 hitpoints_draw:
 loc_0:                              
 tst.w   d0
 ble.s   loc_1
 move.w  #$800D,-4(a5) ; 800D is a tile �of full hitpoint 
 subq.w  #1,d0
 subq.w  #1,d1
 bra.s   loc_3
 loc_1:                              
 tst.w   d1
 ble.s   loc_2
 move.w  #$800E,-4(a5) ; 800E is a tile �of empty hitpoint
 subq.w  #1,d1
 bra.s   loc_3
 loc_2:                               
 move.w  #$8006,-4(a5)
 loc_3:                              
 dbf     d2,loc_0
 jmp     $1F0D8
 
 hitpoints_draw2:
 move.b  (a6)+,d0
 move.b  d0,d1
 andi.w  #$F,d0
 andi.w  #$F0,d1
 asr.w   #4,d1
 moveq   #3,d2
 move.w  #$8F02,(a5)
 move.l  d7,(a5)
 jmp     $1F0CC
