# Contra Super Hard Corps
It's a hack of **Sega Genesis** game *"Contra Hard Corps"* made by *Konami*.

It put few changes to gameplay mechanics, adjust levels, enemies, bosses etc.


Hack by:

* Segaman


Special thanks:

* Ti_

* Dr.Mefisto

* VLD

* 133MHz

* Emu-Land

----------------------------------------------------
## Folders

* **ScrollFix** - a Seg Gopher Console special scroll fix hack

* **HardCore** - Old hack that lower weapon power

* **HitPoints** - Script by Ti_ that adds hitpoints in a US verison of game

* **SuperHardCorps** - Source files of hack



* **bin** - compilers, patchers & compressors

* **LUA** - scripts for emulator debugging

* **out** - binary files of result hacks

----------------------------------------------------
## How to compile

> You can compile this hack only on WinNT systems greater or equal to **Windows XP**

You need a **ROM** file of game named *"Contra - Hard Corps (U).bin"* in the root folder

Start any compile_ *NAME* .bat in a folder, where *NAME* is a name of hack

### For now available hacks:

#### Gopher Scroll Fix
This hack put changes to scroll engine of some levels, so this game can run on **Sega Gopher** without problems.


Known bags:

-Train level boss artifacts

> Do not use this hack anymore, and use **Neto Bootloader** for **Sega Gopher** instead. It give fixes for almost all games

#### HardCore
This hack reduces weapon power. If you want game to be a little bit harder, try this one

#### SuperHardCorps
This is the hack!

#### SuperHardCorps_z80
This will build a new z80 driver for hack, but it is not done and also not working at all.

Just ignore it. Maybe i will finish it later.
